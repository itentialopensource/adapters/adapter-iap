
## 0.2.3 [08-04-2021]

* Update sampleProperties.json

See merge request itentialopensource/adapters/adapter-iap!5

---

## 0.2.2 [03-07-2021]

* migration to the latest adapter foundation - and healthcheck change

See merge request itentialopensource/adapters/adapter-iap!4

---

## 0.2.1 [07-08-2020]

* migration

See merge request itentialopensource/adapters/adapter-iap!2

---

## 0.2.0 [06-03-2020]

* Minor/adapt 199

See merge request itentialopensource/adapters/adapter-iap!1

---

## 0.1.1 [03-26-2020]

* Bug fixes and performance improvements

See commit f84b907

---
