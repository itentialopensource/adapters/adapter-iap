# Itential Automation Platform

Vendor: Itential
Homepage: https://itential.com/

Product: Itential Automation Platform
Product Page: https://www.itential.com/automation-platform/

## Introduction
The Itential Automation Platform Adapter allows you to integrate to other instances of the Itential Automation Platform.

## Why Integrate
The Itential Automation Platform adapter from Itential is used to integrate the Itential Automation Platform (IAP) with other instances of IAP. With this adapter you have the ability to perform operations such as:

- Allows you to integrate to other instances of the Itential Automation Platform (IAP).
- Allows you to trigger automations in IAP.

## Additional Product Documentation
The [API documents for Itential Automation Platform](https://apidocs.itential.com/)
