# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Iap System. The API that was used to build the adapter for Iap is usually available in the report directory of this adapter. The adapter utilizes the Iap API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Itential Automation Platform adapter from Itential is used to integrate the Itential Automation Platform (IAP) with other instances of IAP. With this adapter you have the ability to perform operations such as:

- Allows you to integrate to other instances of the Itential Automation Platform (IAP).
- Allows you to trigger automations in IAP.

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
