## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for IAP. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for IAP.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the IAP. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">createAutomation(name, description, callback)</td>
    <td style="padding:15px">Creates an automation</td>
    <td style="padding:15px">{base_path}/{version}/automation_catalog/automations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAutomations(queryParameters, callback)</td>
    <td style="padding:15px">Gets all known automations</td>
    <td style="padding:15px">{base_path}/{version}/automation_catalog/automations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAutomations(ids, callback)</td>
    <td style="padding:15px">Deletes automations</td>
    <td style="padding:15px">{base_path}/{version}/automation_catalog/automations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAutomationById(id, callback)</td>
    <td style="padding:15px">Gets an single automation by its id</td>
    <td style="padding:15px">{base_path}/{version}/automation_catalog/automations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAutomation(id, options, callback)</td>
    <td style="padding:15px">Updates non-scheduling data for an automation</td>
    <td style="padding:15px">{base_path}/{version}/automation_catalog/automations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">scheduleAutomation(id, options, callback)</td>
    <td style="padding:15px">Updates an automation's schedule</td>
    <td style="padding:15px">{base_path}/{version}/automation_catalog/automations/{pathv1}/schedule?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runAutomation(id, options, callback)</td>
    <td style="padding:15px">Runs an automation outside of it's regularly scheduled runs.
Requires a workflow to be attached to the automation.</td>
    <td style="padding:15px">{base_path}/{version}/automation_catalog/automations/{pathv1}/run?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importAutomations(automations, options, callback)</td>
    <td style="padding:15px">Import automation documents</td>
    <td style="padding:15px">{base_path}/{version}/automation_catalog/automations/import?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exportAutomation(id, callback)</td>
    <td style="padding:15px">Export an automation document</td>
    <td style="padding:15px">{base_path}/{version}/automation_catalog/automations/{pathv1}/export?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">activateWFTask(callback)</td>
    <td style="padding:15px">Activate Task Worker</td>
    <td style="padding:15px">{base_path}/{version}/workflow_engine/activate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addWFWatchers(jobId, watchers, callback)</td>
    <td style="padding:15px">Watch a job for multiple users</td>
    <td style="padding:15px">{base_path}/{version}/workflow_engine/job/watchers/watch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cancelWFJob(jobId, callback)</td>
    <td style="padding:15px">Cancel a job</td>
    <td style="padding:15px">{base_path}/{version}/workflow_engine/cancelJob?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkWorkflowForJobVariables(name, callback)</td>
    <td style="padding:15px">Get Workflow Job Variables</td>
    <td style="padding:15px">{base_path}/{version}/workflow_engine/workflows/variables/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">claimWFTask(taskId, user, callback)</td>
    <td style="padding:15px">Claim a task</td>
    <td style="padding:15px">{base_path}/{version}/workflow_engine/tasks/claim?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createWFJobGroupEntry(id, group, callback)</td>
    <td style="padding:15px">Add Group to Job</td>
    <td style="padding:15px">2019.2:{base_path}/{version}/workflow_engine/jobs/{pathv1}/groups?{query} <br /> 2021.2:{base_path}/{version}/operations-manager/jobs/{pathv1}/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listWFJobGroups(id, callback)</td>
    <td style="padding:15px">List Groups for a Job</td>
    <td style="padding:15px">{base_path}/{version}/workflow_engine/jobs/{pathv1}/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceWFJobGroups(id, groups, callback)</td>
    <td style="padding:15px">Overwrite Groups for a Job</td>
    <td style="padding:15px">2019.2:{base_path}/{version}/workflow_engine/jobs/{pathv1}/groups?{query} <br /> 2021.2:{base_path}/{version}/operations-manager/jobs/{pathv1}/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteWFJobGroups(id, callback)</td>
    <td style="padding:15px">Delete all Groups for a Job</td>
    <td style="padding:15px">2019.2:{base_path}/{version}/workflow_engine/jobs/{pathv1}/groups?{query} <br /> 2021.2:{base_path}/{version}/operations-manager/jobs/{pathv1}/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deactivateWFTask(callback)</td>
    <td style="padding:15px">Deactivate Task Worker</td>
    <td style="padding:15px">{base_path}/{version}/workflow_engine/deactivate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">wfDiffToHTML(label1, value1, label2, value2, callback)</td>
    <td style="padding:15px">diff to HTML</td>
    <td style="padding:15px">{base_path}/{version}/workflow_engine/diffToHTML?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">findWFJob(query, options, callback)</td>
    <td style="padding:15px">Find Job</td>
    <td style="padding:15px">{base_path}/{version}/workflow_engine/jobs/find?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">findWFForwardPaths(startTask, endTask, workflowDetails, callback)</td>
    <td style="padding:15px">Find Forward Paths</td>
    <td style="padding:15px">{base_path}/{version}/workflow_engine/findForwardPaths?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">finishWFManualTask(taskId, jobId, taskData, callback)</td>
    <td style="padding:15px">Complete a manual task</td>
    <td style="padding:15px">{base_path}/{version}/workflow_engine/finishTask?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">fixWFJob(jobId, erroredTask, revertTask, callback)</td>
    <td style="padding:15px">Fix a job</td>
    <td style="padding:15px">{base_path}/{version}/workflow_engine/fixJob?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllWFLoopTasks(workflowDetails, callback)</td>
    <td style="padding:15px">Get All Looped Tasks</td>
    <td style="padding:15px">{base_path}/{version}/workflow_engine/getAllLoopTasks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWFAssociatedJobs(options, callback)</td>
    <td style="padding:15px">Get User's Associated Jobs</td>
    <td style="padding:15px">{base_path}/{version}/workflow_engine/getAssociatedJobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWFEntireJob(jobData, callback)</td>
    <td style="padding:15px">Get Job information Fully</td>
    <td style="padding:15px">{base_path}/{version}/workflow_engine/getEntireJob/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWFJob(jobId, callback)</td>
    <td style="padding:15px">Get Job</td>
    <td style="padding:15px">2019.2:{base_path}/{version}/workflow_engine/getJob/{pathv1}?{query} <br /> 2021.2:{base_path}/{version}/operations-manager/jobs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWFJobDeep(jobId, callback)</td>
    <td style="padding:15px">Get Job Iterations Details</td>
    <td style="padding:15px">{base_path}/{version}/workflow_engine/job/{pathv1}/deep?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWFJobFromTaskQuery(taskQuery, options, callback)</td>
    <td style="padding:15px">Get Jobs From Tasks</td>
    <td style="padding:15px">{base_path}/{version}/workflow_engine/getJobFromTaskQuery?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWFJobList(status, options, callback)</td>
    <td style="padding:15px">Get Jobs by Status</td>
    <td style="padding:15px">{base_path}/{version}/workflow_engine/getJobList/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWFJobShallow(jobData, callback)</td>
    <td style="padding:15px">Get Brief Job Information</td>
    <td style="padding:15px">{base_path}/{version}/workflow_engine/getJobShallow/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWFJobVisualizationData(jobId, callback)</td>
    <td style="padding:15px">Get Job Visualization Data</td>
    <td style="padding:15px">{base_path}/{version}/workflow_engine/jobs/visdata/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWFManualTaskController(jobId, taskId, callback)</td>
    <td style="padding:15px">Get a Manual Task's Controller</td>
    <td style="padding:15px">2019.2:{base_path}/{version}/workflow_engine/tasks/controller/job/{pathv1}/task/{pathv2}?{query} <br /> 2021.2:{base_path}/{version}/operations-manager/jobs/{pathv1}/tasks/{pathv2}/manual-controller?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWFTask(query, filter, callback)</td>
    <td style="padding:15px">Query a Single Task</td>
    <td style="padding:15px">{base_path}/{version}/workflow_engine/getTask?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWFTaskDetails(location, pckg, method, callback)</td>
    <td style="padding:15px">Get Task Details</td>
    <td style="padding:15px">2019.2:{base_path}/{version}/workflow_engine/locations/{pathv1}/packages/{pathv2}/tasks/{pathv3}?{query} <br /> 2022.1:{base_path}/{version}/automation-studio/locations/{pathv1}/packages/{pathv2}/tasks/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWFTaskIterations(jobId, task, callback)</td>
    <td style="padding:15px">Get Iterations of A Task</td>
    <td style="padding:15px">{base_path}/{version}/workflow_engine/getTaskIterations/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWFTaskStatuses(jobId, callback)</td>
    <td style="padding:15px">Get Tasks Status</td>
    <td style="padding:15px">{base_path}/{version}/workflow_engine/job/statuses/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWorkflowsDetailedByName(name, callback)</td>
    <td style="padding:15px">Get Workflow Details</td>
    <td style="padding:15px">2020.2:{base_path}/{version}/workflow_engine/workflows/detailed/{pathv1}?{query} <br /> 2022.1:{base_path}/{version}/automation-studio/workflows/detailed/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">isWFActive(callback)</td>
    <td style="padding:15px">Check Staterator State</td>
    <td style="padding:15px">{base_path}/{version}/workflow_engine/active?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pauseWFJob(jobId, callback)</td>
    <td style="padding:15px">Pause a job</td>
    <td style="padding:15px">{base_path}/{version}/workflow_engine/pauseJob?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">prepareWFMetricsLogs(callback)</td>
    <td style="padding:15px">Prepare the metrics logs tarball</td>
    <td style="padding:15px">{base_path}/{version}/workflow_engine/metrics/jobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">wfQuery(jobId, passOnNull, query, obj, callback)</td>
    <td style="padding:15px">Query data</td>
    <td style="padding:15px">{base_path}/{version}/workflow_engine/query?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">queryWFJobs(query, callback)</td>
    <td style="padding:15px">Query Jobs Collection</td>
    <td style="padding:15px">{base_path}/{version}/workflow_engine/queryJobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">queryWFTasksBrief(query, callback)</td>
    <td style="padding:15px">Query Tasks Collection Brief</td>
    <td style="padding:15px">{base_path}/{version}/workflow_engine/queryTasksBrief?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">releaseWFTask(taskId, callback)</td>
    <td style="padding:15px">Release a task</td>
    <td style="padding:15px">{base_path}/{version}/workflow_engine/tasks/release?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeWFJobGroup(id, group, callback)</td>
    <td style="padding:15px">Remove a Group from a Job</td>
    <td style="padding:15px">2019.2:{base_path}/{version}/workflow_engine/jobs/{pathv1}/groups/{pathv2}?{query} <br /> 2021.2:{base_path}/{version}/operations-manager/jobs/{pathv1}/groups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resumeWFJob(jobId, callback)</td>
    <td style="padding:15px">Resume a job</td>
    <td style="padding:15px">{base_path}/{version}/workflow_engine/resumeJob?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnWFCompletedTaskData(jobId, taskId, taskData, callback)</td>
    <td style="padding:15px">Finish a task</td>
    <td style="padding:15px">{base_path}/{version}/workflow_engine/jobs/finish_task/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">revertToWFTask(jobId, currentTask, targetTask, callback)</td>
    <td style="padding:15px">Revert to a task</td>
    <td style="padding:15px">{base_path}/{version}/workflow_engine/revertToTask?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runWFEvaluationGroup(evaluationGroup, allTrueFlag, callback)</td>
    <td style="padding:15px">Evaluation Group</td>
    <td style="padding:15px">{base_path}/{version}/workflow_engine/runEvaluationGroup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runWFEvaluationGroups(evaluationGroups, allTrueFlag, callback)</td>
    <td style="padding:15px">Evaluation Groups</td>
    <td style="padding:15px">{base_path}/{version}/workflow_engine/runEvaluationGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchWFJobs(options, callback)</td>
    <td style="padding:15px">Search Jobs</td>
    <td style="padding:15px">{base_path}/{version}/workflow_engine/jobs/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchWFTasks(filter, options, callback)</td>
    <td style="padding:15px">Search Tasks</td>
    <td style="padding:15px">{base_path}/{version}/workflow_engine/tasks/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchWFWorkflows(options, callback)</td>
    <td style="padding:15px">Search Workflows</td>
    <td style="padding:15px">{base_path}/{version}/workflow_engine/workflows/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startWFJobWithOptions(workflow, options, callback)</td>
    <td style="padding:15px">Start a Job with Options</td>
    <td style="padding:15px">{base_path}/{version}/workflow_engine/startJobWithOptions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchWFJob(jobId, callback)</td>
    <td style="padding:15px">Watch a job</td>
    <td style="padding:15px">{base_path}/{version}/workflow_engine/job/{pathv1}/watch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unwatchWFJob(jobId, callback)</td>
    <td style="padding:15px">Unwatch a job</td>
    <td style="padding:15px">{base_path}/{version}/workflow_engine/job/{pathv1}/watch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">validateAllWFLoops(workflowDetails, callback)</td>
    <td style="padding:15px">Validate All Loops</td>
    <td style="padding:15px">{base_path}/{version}/workflow_engine/validateAllLoops?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCMDevicesFiltered(options, callback)</td>
    <td style="padding:15px">Find Devices</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCMDevice(name, callback)</td>
    <td style="padding:15px">Get Device Details</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCMDeviceConfig(name, callback)</td>
    <td style="padding:15px">Get Device Configuration</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/devices/{pathv1}/configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCMDeviceConfigFormat(name, format, callback)</td>
    <td style="padding:15px">Get formatted device config (Deprecated)</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/devices/{pathv1}/configuration/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cmBackUpDevice(deviceName, options, callback)</td>
    <td style="padding:15px">Backup device configuration</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/devices/backups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cmImportBackup(backups, callback)</td>
    <td style="padding:15px">Import backup documents</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/import/backups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cmImportGroup(groups, callback)</td>
    <td style="padding:15px">Import device group documents</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/import/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCMBackups(options, callback)</td>
    <td style="padding:15px">Get Device Backup List</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/backups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCMDeviceBackups(backupIds, callback)</td>
    <td style="padding:15px">Delete one or more device backups by Id</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/backups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCMDeviceBackupById(id, callback)</td>
    <td style="padding:15px">Get Device Backup</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/backups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCMDeviceBackupById(id, options, callback)</td>
    <td style="padding:15px">Update a device backup by Id</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/backups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCMDeviceGroupByName(groupName, callback)</td>
    <td style="padding:15px">Get Device Group by Name</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/name/devicegroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cmIsAlive(name, callback)</td>
    <td style="padding:15px">Check if device is connected</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/devices/{pathv1}/isAlive?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCMDeviceGroups(groupDetails, callback)</td>
    <td style="padding:15px">Creates a device group (Deprecated)</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/deviceGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCMDeviceGroups(callback)</td>
    <td style="padding:15px">Get all Device Groups</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/deviceGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCMDeviceGroups(groupIds, callback)</td>
    <td style="padding:15px">Delete device groups</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/deviceGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCMDeviceGroup(groupName, groupDescription, deviceNames, callback)</td>
    <td style="padding:15px">Creates a device group</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/deviceGroup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchCMDeviceGroups(s, start, limit, callback)</td>
    <td style="padding:15px">Search all Device Groups</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/deviceGroups/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCMDeviceGroupById(id, callback)</td>
    <td style="padding:15px">Get Device Group by ID</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/deviceGroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCMDeviceGroups(id, details, callback)</td>
    <td style="padding:15px">Update device groups</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/deviceGroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addCMDevicesToGroup(id, devices, callback)</td>
    <td style="padding:15px">Adds new devices to the group</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/deviceGroups/{pathv1}/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeCMDevicesFromGroup(id, devices, callback)</td>
    <td style="padding:15px">Removes devices from a group</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/deviceGroups/{pathv1}/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCMDeviceGroupsByName(groupNames, callback)</td>
    <td style="padding:15px">Delete device groups</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/name/deviceGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addCMDevicesToGroupByName(groupName, deviceNames, callback)</td>
    <td style="padding:15px">Add device(s) to a group (Deprecated)</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/deviceGroups/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCMDevicesFromGroup(groupName, deviceNames, callback)</td>
    <td style="padding:15px">Delete device(s) from a group (Deprecated)</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/deviceGroups/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cmLookupDiff(id, nextId, collection, nextCollection, options, callback)</td>
    <td style="padding:15px">Diff two strings from a lookup.</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/lookup_diff?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCMGoldenConfigTrees(callback)</td>
    <td style="padding:15px">Get a list of all Golden Config trees.</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/configs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCMGoldenConfigTree(name, deviceType, callback)</td>
    <td style="padding:15px">Adds a new Golden Config tree</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/configs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCMGoldenConfigTrees(treeIds, callback)</td>
    <td style="padding:15px">Delete one or more golden configuration trees by tree id</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/configs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCMGoldenConfigTree(treeId, callback)</td>
    <td style="padding:15px">Get summary of a Golden Config tree.</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/configs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCMGoldenConfigTreeVersion(treeId, version, base, callback)</td>
    <td style="padding:15px">Adds a new Golden Config tree</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/configs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCMGoldenConfigTree(treeId, name, callback)</td>
    <td style="padding:15px">Updates properties of a Golden Config tree</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/configs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCMGoldenConfigTree(treeId, callback)</td>
    <td style="padding:15px">Delete a Golden Config tree.</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/configs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCMGoldenConfigTreeVersion(treeId, version, callback)</td>
    <td style="padding:15px">Get details of a Golden Config tree version.</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/configs/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCMGoldenConfigTreeVersion(treeId, version, name, variables, callback)</td>
    <td style="padding:15px">Updates properties of a Golden Config tree version</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/configs/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCMGoldenConfigTreeVersion(treeId, version, callback)</td>
    <td style="padding:15px">Delete a Golden Config tree.</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/configs/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCMVariables(treeId, version, variables, callback)</td>
    <td style="padding:15px">Deletes one or more variables</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/configs/variables/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exportCMGoldenConfigTree(treeId, callback)</td>
    <td style="padding:15px">Export a Golden Config tree.</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/export/goldenconfigs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importCMGoldenConfigTree(trees, callback)</td>
    <td style="padding:15px">Import golden config tree documents</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/import/goldenconfigs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addCMDevicesToNode(treeId, version, nodePath, devices, callback)</td>
    <td style="padding:15px">Add Devices to Node</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/configs/{pathv1}/{pathv2}/{pathv3}/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeCMDevicesFromNode(treeId, version, nodePath, devices, callback)</td>
    <td style="padding:15px">Remove Devices from Node</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/configs/{pathv1}/{pathv2}/{pathv3}/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCMGoldenConfigNode(treeId, version, parentNodePath, name, callback)</td>
    <td style="padding:15px">Create a new node in a Golden Config tree.</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/configs/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCMGoldenConfigNode(treeId, version, nodePath, name, attributes, callback)</td>
    <td style="padding:15px">Update properties of a node in a Golden Config tree.</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/configs/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCMGoldenConfigNode(treeId, version, nodePath, callback)</td>
    <td style="padding:15px">Delete a node in a Golden Config tree.</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/configs/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCMConfigSpec(id, callback)</td>
    <td style="padding:15px">Get a Config Spec document.</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/config_specs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCMConfigSpec(id, lines, callback)</td>
    <td style="padding:15px">Update a Config Spec.</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/config_specs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCMConfigSpec(deviceType, lines, callback)</td>
    <td style="padding:15px">Create a new Config Spec.</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/config_specs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">translateCMConfigSpec(treeID, version, nodePath, options, callback)</td>
    <td style="padding:15px">Convert a config spec into a readable string.</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/translate/config_spec?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">buildCMSpecLines(nativeConfig, deviceType, options, callback)</td>
    <td style="padding:15px">Builds a config spec from raw config.</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/generate/config_spec?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runCMCompliance(options, callback)</td>
    <td style="padding:15px">Kick off one or more compliance reports.</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/compliance_reports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCMComplianceReportTreeSummary(treeId, callback)</td>
    <td style="padding:15px">Get summary compliance data for a tree.</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/compliance_reports/tree/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCMComplianceReportNodeSummary(treeId, nodePath, callback)</td>
    <td style="padding:15px">Get summary compliance data for a node.</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/compliance_reports/node/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">gradeCMComplianceReports(treeId, version, options, callback)</td>
    <td style="padding:15px">Get graded compliance reports for a tree or node.</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/compliance_reports/grade?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">gradeCMComplianceReport(reportId, options, callback)</td>
    <td style="padding:15px">Get grade of compliance report.</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/compliance_reports/grade/single?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">gradeCMDeviceComplianceHistory(treeId, version, nodePath, deviceName, options, callback)</td>
    <td style="padding:15px">Get historical graded compliance reports.</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/compliance_reports/grade/history?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCMComplianceReportsByBatch(batchId, callback)</td>
    <td style="padding:15px">Get compliance report metadata for a batch.</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/compliance_reports/batch/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCMComplianceReportDeviceHistory(options, callback)</td>
    <td style="padding:15px">Get historical summary compliance data for a device.</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/compliance_reports/history?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCMComplianceReportDetail(reportId, callback)</td>
    <td style="padding:15px">Get details of a particular compliance check.</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/compliance_reports/details/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCMComplianceReportsDetail(reportIds, callback)</td>
    <td style="padding:15px">Get details of a particular set of compliance checks.</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/compliance_reports/details?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopIssues(options, callback)</td>
    <td style="padding:15px">Get the top issues from compliance reports.</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/compliance_reports/topissues?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchCMDeviceConfiguration(deviceName, changes, callback)</td>
    <td style="padding:15px">Alters a device configuration to achieve compliance</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/patch_device/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cmAdapterProxy(device, origin, action, callback)</td>
    <td style="padding:15px">performs all southbound specific logic not caputered by IAP</td>
    <td style="padding:15px">{base_path}/{version}/devices/actions/{pathv1}&/{pathv2}&/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exportMopTemplate(options, type, callback)</td>
    <td style="padding:15px">Export Template</td>
    <td style="padding:15px">{base_path}/{version}/mop/export?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importMopTemplate(template, type, callback)</td>
    <td style="padding:15px">Import Template</td>
    <td style="padding:15px">{base_path}/{version}/mop/import?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listMopTemplates(callback)</td>
    <td style="padding:15px">Get all Command Templates.</td>
    <td style="padding:15px">{base_path}/{version}/mop/listTemplates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listMopTemplateByName(name, callback)</td>
    <td style="padding:15px">Get a Command Template by name.</td>
    <td style="padding:15px">{base_path}/{version}/mop/listATemplate/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listMopAnalyticTemplateByName(name, callback)</td>
    <td style="padding:15px">Get an Analytic Template by Name</td>
    <td style="padding:15px">{base_path}/{version}/mop/listAnAnalyticTemplate/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listMopAnalyticTemplates(callback)</td>
    <td style="padding:15px">Get All Analytic Templates</td>
    <td style="padding:15px">{base_path}/{version}/mop/listAnalyticTemplates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createMopTemplate(mop, callback)</td>
    <td style="padding:15px">Create a Command Template.</td>
    <td style="padding:15px">{base_path}/{version}/mop/createTemplate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createMopAnalyticTemplate(template, callback)</td>
    <td style="padding:15px">Create an Analytic Template</td>
    <td style="padding:15px">{base_path}/{version}/mop/createAnalyticTemplate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteMopTemplate(id, callback)</td>
    <td style="padding:15px">Delete a Command Template</td>
    <td style="padding:15px">{base_path}/{version}/mop/deleteTemplate/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteMopAnalyticTemplate(id, callback)</td>
    <td style="padding:15px">Delete an Analytic Template</td>
    <td style="padding:15px">{base_path}/{version}/mop/deleteAnalyticTemplate/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateMopTemplate(mopID, mop, callback)</td>
    <td style="padding:15px">Update a Command Template</td>
    <td style="padding:15px">{base_path}/{version}/mop/updateTemplate/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateMopAnalyticTemplate(mopID, template, callback)</td>
    <td style="padding:15px">Update an Analytic Template</td>
    <td style="padding:15px">{base_path}/{version}/mop/updateAnalyticTemplate/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runMopCommandDevices(command, variables, devices, callback)</td>
    <td style="padding:15px">Run a Command against Devices</td>
    <td style="padding:15px">{base_path}/{version}/mop/RunCommandDevices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runMopTemplateDevice(template, variables, device, callback)</td>
    <td style="padding:15px">Run a Template against a Devices (Deprecated)</td>
    <td style="padding:15px">{base_path}/{version}/mop/RunTemplateDevice?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runMopTemplateDevices(template, variables, devices, callback)</td>
    <td style="padding:15px">Run a Template against Devices (Deprecated)</td>
    <td style="padding:15px">{base_path}/{version}/mop/RunTemplateDevices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runMopCommand(command, variables, device, callback)</td>
    <td style="padding:15px">Run a Command against a Device</td>
    <td style="padding:15px">{base_path}/{version}/mop/RunCommand?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runMopCommandTemplate(template, variables, devices, callback)</td>
    <td style="padding:15px">Run Command Template</td>
    <td style="padding:15px">{base_path}/{version}/mop/RunCommandTemplate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runMopCommandTemplateSingleCommand(templateId, commandIndex, variables, devices, callback)</td>
    <td style="padding:15px">Run single command from template</td>
    <td style="padding:15px">{base_path}/{version}/mop/RunCommandTemplateSingleCommand?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">mopGetBootFlash(device, callback)</td>
    <td style="padding:15px">MOP Get Boot Flash</td>
    <td style="padding:15px">{base_path}/{version}/mop/GetBootFlash?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">mopSetBoot(device, disk, fileName, ned, deletePrevious, callback)</td>
    <td style="padding:15px">Task to set the boot parameters (Deprecated)</td>
    <td style="padding:15px">{base_path}/{version}/mop/SetBoot?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">mopPassThruCommand(device, command, callback)</td>
    <td style="padding:15px">MOP Pass Thru</td>
    <td style="padding:15px">{base_path}/{version}/mop/passThru/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMopDevicesObjectsFiltered(options, callback)</td>
    <td style="padding:15px">Get Filtered Devices</td>
    <td style="padding:15px">{base_path}/{version}/mop/deviceObjects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMopDevicesFilteredDetailedResults(substring, callback)</td>
    <td style="padding:15px">Get Filtered Devices with detailed query results</td>
    <td style="padding:15px">{base_path}/{version}/mop/devices/detailed/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runMopTemplatesDiffArray(pre, post, callback)</td>
    <td style="padding:15px">MOP Diff Array (Deprecated)</td>
    <td style="padding:15px">{base_path}/{version}/mop/runTemplatesDiffArray?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runMopAnalyticsTemplate(pre, post, analyticTemplateName, variables, callback)</td>
    <td style="padding:15px">Run an Analytics Template</td>
    <td style="padding:15px">{base_path}/{version}/mop/runAnalyticsTemplate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runMopAnalyticsTemplateDevices(pre, post, analyticTemplateName, variables, callback)</td>
    <td style="padding:15px">Run an Analytics Template for Devices</td>
    <td style="padding:15px">{base_path}/{version}/mop/runAnalyticsTemplateDevices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">mopReattempt(jobId, attemptID, minutes, attempts, callback)</td>
    <td style="padding:15px">Re-attempt</td>
    <td style="padding:15px">{base_path}/{version}/mop/reattempt?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMopDevicesFiltered(substring, callback)</td>
    <td style="padding:15px">Get Filtered Devices</td>
    <td style="padding:15px">{base_path}/{version}/mop/devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArtifacts(queryOptions, callback)</td>
    <td style="padding:15px">Get all installed artifacts</td>
    <td style="padding:15px">{base_path}/{version}/admin/artifacts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateArtifact(id, callback)</td>
    <td style="padding:15px">Updates an artifact</td>
    <td style="padding:15px">{base_path}/{version}/admin/artifacts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeArtifact(id, callback)</td>
    <td style="padding:15px">Remove an artifact</td>
    <td style="padding:15px">{base_path}/{version}/admin/artifacts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importArtifact(artifact, callback)</td>
    <td style="padding:15px">Import an artifact</td>
    <td style="padding:15px">{base_path}/{version}/admin/artifacts/import?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">undiscoverAll(callback)</td>
    <td style="padding:15px">undiscoverAll</td>
    <td style="padding:15px">{base_path}/{version}/ag-manager/actions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllAdaptersAllClusters(callback)</td>
    <td style="padding:15px">getAllAdaptersAllClusters</td>
    <td style="padding:15px">{base_path}/{version}/ag-manager/getClusterAdapters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">discoverModules(adapterId, callback)</td>
    <td style="padding:15px">discoverModules</td>
    <td style="padding:15px">{base_path}/{version}/ag-manager/actions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">undiscoverModules(adapterId, callback)</td>
    <td style="padding:15px">undiscoverModules</td>
    <td style="padding:15px">{base_path}/{version}/ag-manager/actions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPageOfTemplateDocuments(limit, skip, order, sort, include, exclude, inParam, notIn, equals, contains, startsWith, endsWith, callback)</td>
    <td style="padding:15px">getPageOfTemplateDocuments.</td>
    <td style="padding:15px">{base_path}/{version}/automation-studio/templates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNewTemplateDocument(body, callback)</td>
    <td style="padding:15px">createNewTemplateDocument.</td>
    <td style="padding:15px">{base_path}/{version}/automation-studio/templates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importNewTemplateDocument(body, callback)</td>
    <td style="padding:15px">importNewTemplateDocument.</td>
    <td style="padding:15px">{base_path}/{version}/automation-studio/templates/import?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllAvailableRestCallsIAP(callback)</td>
    <td style="padding:15px">getAllAvailableRestCallsIAP.</td>
    <td style="padding:15px">{base_path}/{version}/automation-studio/json-forms/method-options?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getListOfAllAppsAndAdapters(callback)</td>
    <td style="padding:15px">getListOfAllAppsAndAdapters</td>
    <td style="padding:15px">{base_path}/{version}/automation-studio/apps/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPageOfWorkflowDocuments(limit, skip, order, sort, include, exclude, expand, inParam, notIn, equals, contains, startsWith, endsWith, callback)</td>
    <td style="padding:15px">getPageOfWorkflowDocuments.</td>
    <td style="padding:15px">{base_path}/{version}/automation-studio/workflows?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNewWorkflowDocument(body, callback)</td>
    <td style="padding:15px">createNewWorkflowDocument.</td>
    <td style="padding:15px">{base_path}/{version}/automation-studio/automations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importNewWorkflowDocument(body, callback)</td>
    <td style="padding:15px">importNewWorkflowDocument.</td>
    <td style="padding:15px">{base_path}/{version}/automation-studio/automations/import?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPageOfComponentGroupDocuments(limit, skip, order, sort, include, exclude, inParam, notIn, equals, contains, startsWith, endsWith, callback)</td>
    <td style="padding:15px">getPageOfComponentGroupDocuments.</td>
    <td style="padding:15px">{base_path}/{version}/automation-studio/component-groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNewComponentGroupDocument(body, callback)</td>
    <td style="padding:15px">createNewComponentGroupDocument.</td>
    <td style="padding:15px">{base_path}/{version}/automation-studio/component-groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importNewComponentGroupDocument(body, callback)</td>
    <td style="padding:15px">importNewComponentGroupDocument.</td>
    <td style="padding:15px">{base_path}/{version}/automation-studio/component-groups/import?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">validateWorkflow(body, callback)</td>
    <td style="padding:15px">validateWorkflow</td>
    <td style="padding:15px">{base_path}/{version}/automation-studio/workflows/validate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReferencesToParticularDocument(targetType, targetIdentifiers, referrerType, callback)</td>
    <td style="padding:15px">getReferencesToParticularDocument.</td>
    <td style="padding:15px">{base_path}/{version}/automation-studio/references-to?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMultipleTaskDetails(body, callback)</td>
    <td style="padding:15px">getMultipleTaskDetails</td>
    <td style="padding:15px">{base_path}/{version}/automation-studio/multipleTaskDetails?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSingleTemplateDocument(id, include, exclude, callback)</td>
    <td style="padding:15px">getSingleTemplateDocument.</td>
    <td style="padding:15px">{base_path}/{version}/automation-studio/templates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTemplateDocument(id, callback)</td>
    <td style="padding:15px">deleteTemplateDocument.</td>
    <td style="padding:15px">{base_path}/{version}/automation-studio/templates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceTemplateDocument(id, body, callback)</td>
    <td style="padding:15px">replaceTemplateDocument.</td>
    <td style="padding:15px">{base_path}/{version}/automation-studio/templates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exportTemplateDocument(id, callback)</td>
    <td style="padding:15px">exportTemplateDocument.</td>
    <td style="padding:15px">{base_path}/{version}/automation-studio/templates/{pathv1}/export?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">transformDataUsingJSTTransformationDocument(transformationId, body, callback)</td>
    <td style="padding:15px">transformDataUsingJSTTransformationDocument.</td>
    <td style="padding:15px">{base_path}/{version}/automation-studio/json-forms/runTransformation/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDetailsOfWorkflow(name, callback)</td>
    <td style="padding:15px">getDetailsOfWorkflow</td>
    <td style="padding:15px">{base_path}/{version}/automation-studio/workflows/detailed/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceWorkflowDocument(id, body, callback)</td>
    <td style="padding:15px">replaceWorkflowDocument.</td>
    <td style="padding:15px">{base_path}/{version}/automation-studio/automations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getComponentGroup(id, include, exclude, callback)</td>
    <td style="padding:15px">getComponentGroup.</td>
    <td style="padding:15px">{base_path}/{version}/automation-studio/component-groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteComponentGroupDocument(id, callback)</td>
    <td style="padding:15px">deleteComponentGroupDocument.</td>
    <td style="padding:15px">{base_path}/{version}/automation-studio/component-groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceComponentGroupDocument(id, body, callback)</td>
    <td style="padding:15px">replaceComponentGroupDocument.</td>
    <td style="padding:15px">{base_path}/{version}/automation-studio/component-groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">migrateAgendaJobs(body, callback)</td>
    <td style="padding:15px">migrateAgendaJobs</td>
    <td style="padding:15px">{base_path}/{version}/automation_catalog/automations/migration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">fetchesOSTypesToStoreIntoCache(body, callback)</td>
    <td style="padding:15px">fetchesOSTypesToStoreIntoCache.</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/cache/devices/ostype?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">fetchesOSTypesStoredCache(callback)</td>
    <td style="padding:15px">fetchesOSTypesStoredCache.</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/cache/devices/ostype?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCacheForOsTypes(callback)</td>
    <td style="padding:15px">deleteCacheForOsTypes.</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/cache/devices/ostype?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePinnedItems(callback)</td>
    <td style="padding:15px">deletePinnedItems.</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/pins?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">handlePin(body, callback)</td>
    <td style="padding:15px">handlePin</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/pins?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getListOfPinnedItems(body, callback)</td>
    <td style="padding:15px">getListOfPinnedItems.</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/pins/fetch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">renderJinja2Template(body, callback)</td>
    <td style="padding:15px">renderJinja2Template.</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/jinja2?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDataForAllAdaptersWithTasks(body, callback)</td>
    <td style="padding:15px">getDataForAllAdaptersWithTasks.</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/adaptertasks/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDataForSpecificAdapterTask(body, callback)</td>
    <td style="padding:15px">getDataForSpecificAdapterTask.</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/adaptertasks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTaskInstance(body, callback)</td>
    <td style="padding:15px">createTaskInstance.</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/tasks/instance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTaskInstance(body, callback)</td>
    <td style="padding:15px">updateTaskInstance.</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/tasks/instance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTaskInstanceS(callback)</td>
    <td style="padding:15px">deleteTaskInstance(s).</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/tasks/instance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTaskInstances(body, callback)</td>
    <td style="padding:15px">getTaskInstance(s).</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/tasks/instances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runTaskInstance(body, callback)</td>
    <td style="padding:15px">runTaskInstance.</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/tasks/instance/run?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runAnAdapterTask(body, callback)</td>
    <td style="padding:15px">runAnAdapterTask.</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/tasks/run?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchDeviceGroups(body, callback)</td>
    <td style="padding:15px">searchDeviceGroups</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/deviceGroups/query?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createConfigParser(body, callback)</td>
    <td style="padding:15px">createConfigParser.</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/configurations/parser?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateConfigParser(body, callback)</td>
    <td style="padding:15px">updateConfigParser.</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/configurations/parser?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigParser(callback)</td>
    <td style="padding:15px">deleteConfigParser.</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/configurations/parser?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllConfigParsers(start, limit, regex, filter, sort, callback)</td>
    <td style="padding:15px">getAllConfigParsers.</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/configurations/parser?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigParser(body, callback)</td>
    <td style="padding:15px">getConfigParser.</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/configurations/parser/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOneOrMoreConfigParsersById(callback)</td>
    <td style="padding:15px">deleteOneOrMoreConfigParsersById</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/configurations/parsers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importParserDocuments(body, callback)</td>
    <td style="padding:15px">importParserDocuments</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/import/parsers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchGoldenConfigTrees(body, callback)</td>
    <td style="padding:15px">searchGoldenConfigTrees.</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/search/configs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">findTreesWhichContainSpecifiedDevice(body, callback)</td>
    <td style="padding:15px">findTreesWhichContainSpecifiedDevice.</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/devices/device/trees?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">fetchAllDevicesThatExistOnTree(body, callback)</td>
    <td style="padding:15px">fetchAllDevicesThatExistOnTree.</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/devices/tree?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateJSONConfigRules(body, callback)</td>
    <td style="padding:15px">updateJSONConfigRules</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/configurations/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeDeviceGroupsFromNode(callback)</td>
    <td style="padding:15px">removeDeviceGroupsFromNode</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/configs/devices/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addDeviceGroupsToNode(body, callback)</td>
    <td style="padding:15px">addDeviceGroupsToNode</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/configs/devices/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addTasksToNode(body, callback)</td>
    <td style="padding:15px">addTasksToNode</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/configs/node/tasks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeTasksFromNode(callback)</td>
    <td style="padding:15px">removeTasksFromNode</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/configs/node/tasks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigSpecTemplate(body, callback)</td>
    <td style="padding:15px">getConfigSpecTemplate.</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/config_template?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNodeConfiguration(body, callback)</td>
    <td style="padding:15px">updateNodeConfiguration.</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/node/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">convertConfigSpecIntoReadableString(body, callback)</td>
    <td style="padding:15px">convertConfigSpecIntoReadableString.</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/translate/config_spec?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJSONSpecDocumentWithInheritance(body, callback)</td>
    <td style="padding:15px">getJSONSpecDocumentWithInheritance.</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/json_specs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNewJSONSpec(body, callback)</td>
    <td style="padding:15px">createNewJSONSpec.</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/json_specs/create?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runComplianceReports(body, callback)</td>
    <td style="padding:15px">runComplianceReports.</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/compliance_reports/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runComplianceReportsOnBackups(body, callback)</td>
    <td style="padding:15px">runComplianceReportsOnBackups.</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/compliance_reports/backups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHistoricalGradedComplianceReports(body, callback)</td>
    <td style="padding:15px">getHistoricalGradedComplianceReports.</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/json_compliance_reports/grade/history?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">complianceReportsTotalsForBackup(body, callback)</td>
    <td style="padding:15px">complianceReportsTotalsForBackup.</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/compliance_reports/history/backups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">complianceReportsTotalsForTaskInstance(body, callback)</td>
    <td style="padding:15px">complianceReportsTotalsForTaskInstance.</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/json_compliance_reports/history?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopIssuesFromJSONComplianceReports(body, callback)</td>
    <td style="padding:15px">getTopIssuesFromJSONComplianceReports</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/json_compliance_reports/topissues?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">convertPatchDataToNativeConfig(body, callback)</td>
    <td style="padding:15px">convertPatchDataToNativeConfig.</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/changes/convert?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDeviceTemplate(body, callback)</td>
    <td style="padding:15px">createDeviceTemplate</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/templates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDeviceTemplate(body, callback)</td>
    <td style="padding:15px">updateDeviceTemplate</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/templates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOneOrMoreDeviceTemplateSById(callback)</td>
    <td style="padding:15px">deleteOneOrMoreDeviceTemplate(s)ById</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/templates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceTemplate(body, callback)</td>
    <td style="padding:15px">getDeviceTemplate</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/templates/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importDeviceConfigTemplateDocuments(body, callback)</td>
    <td style="padding:15px">importDeviceConfigTemplateDocuments</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/import/templates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applyDeviceConfigTemplate(body, callback)</td>
    <td style="padding:15px">applyDeviceConfigTemplate</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/templates/apply?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJSONSpecDocument(id, callback)</td>
    <td style="padding:15px">getJSONSpecDocument.</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/json_specs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateJSONSpec(id, body, callback)</td>
    <td style="padding:15px">updateJSONSpec</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/json_specs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getComplianceReportMetadataForBatch(batchId, callback)</td>
    <td style="padding:15px">getComplianceReportMetadataForBatch.</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/json_compliance_reports/batch/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJSONComplianceReport(reportId, callback)</td>
    <td style="padding:15px">getJSONComplianceReport</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/json_compliance_reports/details/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applyDeviceConfig(deviceName, body, callback)</td>
    <td style="padding:15px">applyDeviceConfig</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/devices/{pathv1}/configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOperationalDataForDevice(deviceName, body, callback)</td>
    <td style="padding:15px">getOperationalDataForDevice</td>
    <td style="padding:15px">{base_path}/{version}/configuration_manager/devices/{pathv1}/operationaldata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">saveGeneratedForm(body, callback)</td>
    <td style="padding:15px">saveGeneratedForm</td>
    <td style="padding:15px">{base_path}/{version}/formbuilder/saveForm?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">preserveIncomingFormDataFormat(body, callback)</td>
    <td style="padding:15px">preserveIncomingFormDataFormat</td>
    <td style="padding:15px">{base_path}/{version}/formbuilder/preserveFormData?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAllFormNames(callback)</td>
    <td style="padding:15px">listAllFormNames</td>
    <td style="padding:15px">{base_path}/{version}/formbuilder/listForms?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchForms(body, callback)</td>
    <td style="padding:15px">searchForms</td>
    <td style="padding:15px">{base_path}/{version}/formbuilder/forms/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceInstanceData(body, callback)</td>
    <td style="padding:15px">getServiceInstanceData</td>
    <td style="padding:15px">{base_path}/{version}/formbuilder/fetchData?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAllElementsUsableForms(callback)</td>
    <td style="padding:15px">listAllElementsUsableForms</td>
    <td style="padding:15px">{base_path}/{version}/formbuilder/listElements?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exportForm(body, callback)</td>
    <td style="padding:15px">exportForm</td>
    <td style="padding:15px">{base_path}/{version}/formbuilder/forms/export?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importForm(body, callback)</td>
    <td style="padding:15px">importForm</td>
    <td style="padding:15px">{base_path}/{version}/formbuilder/forms/import?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDetailsOfFormByName(formName, callback)</td>
    <td style="padding:15px">getDetailsOfFormByName</td>
    <td style="padding:15px">{base_path}/{version}/formbuilder/getFormByName/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDetailsOfFormByID(formId, callback)</td>
    <td style="padding:15px">getDetailsOfFormByID</td>
    <td style="padding:15px">{base_path}/{version}/formbuilder/getForm/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFormByID(id, callback)</td>
    <td style="padding:15px">deleteFormByID</td>
    <td style="padding:15px">{base_path}/{version}/formbuilder/deleteForm/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listGroupsForForm(name, callback)</td>
    <td style="padding:15px">listGroupsForForm</td>
    <td style="padding:15px">{base_path}/{version}/formbuilder/forms/{pathv1}/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">overwriteGroupsForForm(name, body, callback)</td>
    <td style="padding:15px">overwriteGroupsForForm</td>
    <td style="padding:15px">{base_path}/{version}/formbuilder/forms/{pathv1}/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addGroupToForm(name, body, callback)</td>
    <td style="padding:15px">addGroupToForm</td>
    <td style="padding:15px">{base_path}/{version}/formbuilder/forms/{pathv1}/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFormGroups(name, callback)</td>
    <td style="padding:15px">deleteFormGroups</td>
    <td style="padding:15px">{base_path}/{version}/formbuilder/forms/{pathv1}/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAnElementDefinition(type, id, callback)</td>
    <td style="padding:15px">getAnElementDefinition.</td>
    <td style="padding:15px">{base_path}/{version}/formbuilder/getElementDefinition/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeGroupFromForm(name, group, callback)</td>
    <td style="padding:15px">removeGroupFromForm</td>
    <td style="padding:15px">{base_path}/{version}/formbuilder/forms/{pathv1}/groups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createJsonForm(body, callback)</td>
    <td style="padding:15px">createJsonForm</td>
    <td style="padding:15px">{base_path}/{version}/json-forms/forms?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnAllForms(callback)</td>
    <td style="padding:15px">returnAllForms</td>
    <td style="padding:15px">{base_path}/{version}/json-forms/forms?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteForms(callback)</td>
    <td style="padding:15px">deleteForms</td>
    <td style="padding:15px">{base_path}/{version}/json-forms/forms?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importFormDocuments(body, callback)</td>
    <td style="padding:15px">importFormDocuments</td>
    <td style="padding:15px">{base_path}/{version}/json-forms/import/forms?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">validateDataAgainstSchema(body, callback)</td>
    <td style="padding:15px">validateDataAgainstSchema.</td>
    <td style="padding:15px">{base_path}/{version}/json-forms/validate-data?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">convertYANGToJSONSchema(body, callback)</td>
    <td style="padding:15px">convertYANGToJSONSchema.</td>
    <td style="padding:15px">{base_path}/{version}/json-forms/yangToSchema?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">decodeAnEncodedJSONSchema(body, callback)</td>
    <td style="padding:15px">decodeAnEncodedJSONSchema.</td>
    <td style="padding:15px">{base_path}/{version}/json-forms/decode?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">findForm(id, callback)</td>
    <td style="padding:15px">findForm</td>
    <td style="padding:15px">{base_path}/{version}/json-forms/forms/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateForm(id, body, callback)</td>
    <td style="padding:15px">updateForm</td>
    <td style="padding:15px">{base_path}/{version}/json-forms/forms/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">validateFormDataAgainstItsSchema(id, body, callback)</td>
    <td style="padding:15px">validateFormDataAgainstItsSchema</td>
    <td style="padding:15px">{base_path}/{version}/json-forms/validate-form/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNewResourceModel(body, callback)</td>
    <td style="padding:15px">createNewResourceModel</td>
    <td style="padding:15px">{base_path}/{version}/lifecycle-manager/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importResourceModel(body, callback)</td>
    <td style="padding:15px">importResourceModel</td>
    <td style="padding:15px">{base_path}/{version}/lifecycle-manager/resources/import?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteResourceModel(id, callback)</td>
    <td style="padding:15px">deleteResourceModel</td>
    <td style="padding:15px">{base_path}/{version}/lifecycle-manager/resources/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getResourceModelBasedOnId(id, dereference, callback)</td>
    <td style="padding:15px">getResourceModelBasedOnId</td>
    <td style="padding:15px">{base_path}/{version}/lifecycle-manager/resources/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateResourceModel(id, body, callback)</td>
    <td style="padding:15px">updateResourceModel</td>
    <td style="padding:15px">{base_path}/{version}/lifecycle-manager/resources/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editResourceModelHttp(modelId, body, callback)</td>
    <td style="padding:15px">editResourceModelHttp</td>
    <td style="padding:15px">{base_path}/{version}/lifecycle-manager/resources/{pathv1}/edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exportResourceModel(modelId, callback)</td>
    <td style="padding:15px">exportResourceModel</td>
    <td style="padding:15px">{base_path}/{version}/lifecycle-manager/resources/{pathv1}/export?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSingleActionExecutionRecord(id, sync, callback)</td>
    <td style="padding:15px">getSingleActionExecutionRecord</td>
    <td style="padding:15px">{base_path}/{version}/lifecycle-manager/action-executions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runResourceAction(modelId, body, callback)</td>
    <td style="padding:15px">runResourceAction</td>
    <td style="padding:15px">{base_path}/{version}/lifecycle-manager/resources/{pathv1}/run-action?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">validateActionsDefinedOnResourceModel(modelId, body, callback)</td>
    <td style="padding:15px">validateActionsDefinedOnResourceModel</td>
    <td style="padding:15px">{base_path}/{version}/lifecycle-manager/resources/{pathv1}/actions/validate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSingleResourceInstance(modelId, instanceId, callback)</td>
    <td style="padding:15px">getSingleResourceInstance</td>
    <td style="padding:15px">{base_path}/{version}/lifecycle-manager/resources/{pathv1}/instances/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateInstanceMetadataHttp(modelId, instanceId, body, callback)</td>
    <td style="padding:15px">update`name`And`description`FieldsOfResourceInstance</td>
    <td style="padding:15px">{base_path}/{version}/lifecycle-manager/resources/{pathv1}/instances/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">findDiffOfTwoConfigStrings(body, callback)</td>
    <td style="padding:15px">findDiffOfTwoConfigStrings.</td>
    <td style="padding:15px">{base_path}/{version}/mop/diff?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJSONSchemasOfServiceModels(body, callback)</td>
    <td style="padding:15px">getJSONSchemasOfServiceModels.</td>
    <td style="padding:15px">{base_path}/{version}/nso_manager/serviceModel/schemas?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runLiveStatusCommandAgainstListOfDevices(body, callback)</td>
    <td style="padding:15px">runLiveStatusCommandAgainstListOfDevices</td>
    <td style="padding:15px">{base_path}/{version}/nso_manager/runCommand?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runListOfLiveStatusCommandsAgainstListOfDevices(body, callback)</td>
    <td style="padding:15px">runListOfLiveStatusCommandsAgainstListOfDevices</td>
    <td style="padding:15px">{base_path}/{version}/nso_manager/runCommands?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runLiveStatus(body, callback)</td>
    <td style="padding:15px">runLiveStatus</td>
    <td style="padding:15px">{base_path}/{version}/nso_manager/liveStatus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runIsLive(body, callback)</td>
    <td style="padding:15px">runIsLive</td>
    <td style="padding:15px">{base_path}/{version}/nso_manager/isAlive?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applytemplateTask(body, callback)</td>
    <td style="padding:15px">applytemplateTask</td>
    <td style="padding:15px">{base_path}/{version}/nso_manager/applyTemplates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllNEDsNSO(callback)</td>
    <td style="padding:15px">getAllNEDsNSO</td>
    <td style="padding:15px">{base_path}/{version}/nso_manager/allNeds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllAuthenticationGroups(callback)</td>
    <td style="padding:15px">getAllAuthenticationGroups</td>
    <td style="padding:15px">{base_path}/{version}/nso_manager/allAuthgroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRollbackFiles(body, callback)</td>
    <td style="padding:15px">getRollbackFilesBasedOnLabel/comment/fixed-number.</td>
    <td style="padding:15px">{base_path}/{version}/nso_manager/getRollbackFiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLeafrefValuesFromNSO(body, callback)</td>
    <td style="padding:15px">getLeafrefValuesFromNSO</td>
    <td style="padding:15px">{base_path}/{version}/nso_manager/getLeafrefValues?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">evaluateWhen(body, callback)</td>
    <td style="padding:15px">evaluateWhen.</td>
    <td style="padding:15px">{base_path}/{version}/nso_manager/evaluateWhen?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">validateCommit(body, callback)</td>
    <td style="padding:15px">validateCommit.</td>
    <td style="padding:15px">{base_path}/{version}/nso_manager/validateCommit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCommitQueueItems(adapterId, callback)</td>
    <td style="padding:15px">getCommitQueueItems</td>
    <td style="padding:15px">{base_path}/{version}/nso_manager/getCommitQueueDeep/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addLockItem(adapterId, body, callback)</td>
    <td style="padding:15px">addLockItem</td>
    <td style="padding:15px">{base_path}/{version}/nso_manager/lockItem/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pruneDevicesFromAllItems(adapterId, body, callback)</td>
    <td style="padding:15px">pruneDevicesFromAllItems</td>
    <td style="padding:15px">{base_path}/{version}/nso_manager/queueItems/prune/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllQueuedDevices(adapterId, callback)</td>
    <td style="padding:15px">getAllQueuedDevices</td>
    <td style="padding:15px">{base_path}/{version}/nso_manager/queuedDevices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setleafTask(adapterId, body, callback)</td>
    <td style="padding:15px">setleafTask</td>
    <td style="padding:15px">{base_path}/{version}/nso_manager/setLeaf/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runAction(adapterId, body, callback)</td>
    <td style="padding:15px">runAction</td>
    <td style="padding:15px">{base_path}/{version}/nso_manager/runAction/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setitemnacmgroupTask(adapterId, body, callback)</td>
    <td style="padding:15px">setitemnacmgroupTask</td>
    <td style="padding:15px">{base_path}/{version}/nso_manager/setItemNacmGroup/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNEDsNSO(adapterId, callback)</td>
    <td style="padding:15px">getNEDsNSO</td>
    <td style="padding:15px">{base_path}/{version}/nso_manager/neds/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllNEDsDetailed(adapterId, callback)</td>
    <td style="padding:15px">getAllNEDsDetailed</td>
    <td style="padding:15px">{base_path}/{version}/nso_manager/neds/deep/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthenticationGroups(adapterId, callback)</td>
    <td style="padding:15px">getAuthenticationGroups</td>
    <td style="padding:15px">{base_path}/{version}/nso_manager/authgroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">restqueryTask(adapterId, body, callback)</td>
    <td style="padding:15px">restqueryTask</td>
    <td style="padding:15px">{base_path}/{version}/nso_manager/restQuery/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">makeRESTActionsToNSO(adapterId, body, callback)</td>
    <td style="padding:15px">makeRESTActionsToNSO</td>
    <td style="padding:15px">{base_path}/{version}/nso_manager/restAction/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFilteredDevices(substring, callback)</td>
    <td style="padding:15px">getFilteredDevices</td>
    <td style="padding:15px">{base_path}/{version}/nso_manager/devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">validateNetworkConfiguration(adapter, nedid, body, callback)</td>
    <td style="padding:15px">validateNetworkConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/nso_manager/verifyConfig/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteQueueItem(queueItemId, adapterId, callback)</td>
    <td style="padding:15px">deleteQueueItem</td>
    <td style="padding:15px">{base_path}/{version}/nso_manager/queueItem/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">lockQueueItem(queueItemId, adapterId, callback)</td>
    <td style="padding:15px">lockQueueItem</td>
    <td style="padding:15px">{base_path}/{version}/nso_manager/queueItem/lock/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unlockQueueItem(queueItemId, adapterId, callback)</td>
    <td style="padding:15px">unlockQueueItem</td>
    <td style="padding:15px">{base_path}/{version}/nso_manager/queueItem/unlock/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pruneDevicesFromAnItem(queueItemId, adapterId, body, callback)</td>
    <td style="padding:15px">pruneDevicesFromAnItem</td>
    <td style="padding:15px">{base_path}/{version}/nso_manager/queueItem/prune/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getItemSDetails(adapterId, queueItemId, callback)</td>
    <td style="padding:15px">getItem'sDetails</td>
    <td style="padding:15px">{base_path}/{version}/nso_manager/queueItem/detailed/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFilteredDevicesFromAnAdapter(substring, lockedAdapter, callback)</td>
    <td style="padding:15px">getFilteredDevicesFromAnAdapter</td>
    <td style="padding:15px">{base_path}/{version}/nso_manager/devices/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cloneAutomation(body, callback)</td>
    <td style="padding:15px">cloneAutomation</td>
    <td style="padding:15px">{base_path}/{version}/operations-manager/automations/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOperationsManagerAutomation(body, callback)</td>
    <td style="padding:15px">createAutomation</td>
    <td style="padding:15px">{base_path}/{version}/operations-manager/automations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOperationsManagerAutomations(contains, containsField, equals, equalsField, startsWith, startsWithField, limit, skip, order, sort, callback)</td>
    <td style="padding:15px">getAutomations</td>
    <td style="padding:15px">{base_path}/{version}/operations-manager/automations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importOperationsManagerAutomations(body, callback)</td>
    <td style="padding:15px">importAutomations</td>
    <td style="padding:15px">{base_path}/{version}/operations-manager/automations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTriggers(contains, containsField, equals, equalsField, startsWith, startsWithField, greaterThanField, greaterThan, lessThanField, lessThan, enabled, actionId, limit, skip, order, sort, callback)</td>
    <td style="padding:15px">getTriggers</td>
    <td style="padding:15px">{base_path}/{version}/operations-manager/triggers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTrigger(body, callback)</td>
    <td style="padding:15px">createTrigger</td>
    <td style="padding:15px">{base_path}/{version}/operations-manager/triggers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importTriggers(body, callback)</td>
    <td style="padding:15px">importTriggers</td>
    <td style="padding:15px">{base_path}/{version}/operations-manager/triggers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEvents(source, topic, limit, skip, sort, callback)</td>
    <td style="padding:15px">OperationsManagerEvents_GET</td>
    <td style="padding:15px">{base_path}/{version}/operations-manager/events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPageOfTaskDocumentsBypassingGBAC(limit, skip, order, sort, include, exclude, inParam, notIn, equals, contains, startsWith, endsWith, dereference, gt, gte, lt, lte, q, callback)</td>
    <td style="padding:15px">getPageOfTaskDocuments,BypassingGBAC.</td>
    <td style="padding:15px">{base_path}/{version}/operations-manager/tasks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unwatchJobs(body, callback)</td>
    <td style="padding:15px">unwatchJobs</td>
    <td style="padding:15px">{base_path}/{version}/operations-manager/jobs/unwatch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchJobs(body, callback)</td>
    <td style="padding:15px">watchJobs</td>
    <td style="padding:15px">{base_path}/{version}/operations-manager/jobs/watch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPageOfJobDocuments(limit, skip, order, sort, include, exclude, inParam, notIn, equals, contains, startsWith, endsWith, dereference, gt, gte, lt, lte, q, callback)</td>
    <td style="padding:15px">getPageOfJobDocuments.</td>
    <td style="padding:15px">{base_path}/{version}/operations-manager/jobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startJob(body, callback)</td>
    <td style="padding:15px">startJob.</td>
    <td style="padding:15px">{base_path}/{version}/operations-manager/jobs/start?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pauseJobs(body, callback)</td>
    <td style="padding:15px">pauseJobs.</td>
    <td style="padding:15px">{base_path}/{version}/operations-manager/jobs/pause?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resumeJobs(body, callback)</td>
    <td style="padding:15px">resumeJobs.</td>
    <td style="padding:15px">{base_path}/{version}/operations-manager/jobs/resume?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cancelJobs(body, callback)</td>
    <td style="padding:15px">cancelJobs.</td>
    <td style="padding:15px">{base_path}/{version}/operations-manager/jobs/cancel?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOperationsManagerAutomation(id, callback)</td>
    <td style="padding:15px">deleteOperationsManagerAutomation</td>
    <td style="padding:15px">{base_path}/{version}/operations-manager/automations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOperationsManagerAutomation(id, callback)</td>
    <td style="padding:15px">getAutomation</td>
    <td style="padding:15px">{base_path}/{version}/operations-manager/automations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOperationsManagerAutomation(id, body, callback)</td>
    <td style="padding:15px">updateAutomation</td>
    <td style="padding:15px">{base_path}/{version}/operations-manager/automations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exportOperationsManagerAutomation(id, callback)</td>
    <td style="padding:15px">exportAutomation</td>
    <td style="padding:15px">{base_path}/{version}/operations-manager/automations/{pathv1}/export?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTrigger(id, callback)</td>
    <td style="padding:15px">getTrigger</td>
    <td style="padding:15px">{base_path}/{version}/operations-manager/triggers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTrigger(id, body, callback)</td>
    <td style="padding:15px">updateTrigger</td>
    <td style="padding:15px">{base_path}/{version}/operations-manager/triggers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTrigger(id, callback)</td>
    <td style="padding:15px">deleteTrigger</td>
    <td style="padding:15px">{base_path}/{version}/operations-manager/triggers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTriggersByActionID(id, callback)</td>
    <td style="padding:15px">deleteTriggersByActionID</td>
    <td style="padding:15px">{base_path}/{version}/operations-manager/triggers/action/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exportTrigger(id, callback)</td>
    <td style="padding:15px">exportTrigger</td>
    <td style="padding:15px">{base_path}/{version}/operations-manager/triggers/{pathv1}/export?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runEndpointTriggerWithPOST(routeName, body, callback)</td>
    <td style="padding:15px">runEndpointTriggerWithPOST</td>
    <td style="padding:15px">{base_path}/{version}/operations-manager/triggers/endpoint/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runManualTrigger(id, body, callback)</td>
    <td style="padding:15px">runManualTrigger</td>
    <td style="padding:15px">{base_path}/{version}/operations-manager/triggers/manual/{pathv1}/run?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">claimTaskOpsMgr(taskId, callback)</td>
    <td style="padding:15px">claimTaskOpsMgr</td>
    <td style="padding:15px">{base_path}/{version}/operations-manager/tasks/{pathv1}/claim?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignTaskToUser(taskId, body, callback)</td>
    <td style="padding:15px">assignTaskToUser</td>
    <td style="padding:15px">{base_path}/{version}/operations-manager/tasks/{pathv1}/assign?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">releaseCurrentlyAssignedTask(taskId, callback)</td>
    <td style="padding:15px">releaseCurrentlyAssignedTask</td>
    <td style="padding:15px">{base_path}/{version}/operations-manager/tasks/{pathv1}/release?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTaskByID(id, include, exclude, dereference, callback)</td>
    <td style="padding:15px">getTaskByID</td>
    <td style="padding:15px">{base_path}/{version}/operations-manager/tasks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchOpsMgrJob(jobId, callback)</td>
    <td style="padding:15px">watchOpsMgrJob</td>
    <td style="padding:15px">{base_path}/{version}/operations-manager/jobs/{pathv1}/watch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unwatchOpsMgrJob(jobId, callback)</td>
    <td style="padding:15px">unwatchOpsMgrJob</td>
    <td style="padding:15px">{base_path}/{version}/operations-manager/jobs/{pathv1}/unwatch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addWatchersToJobOpsMgr(jobId, body, callback)</td>
    <td style="padding:15px">addWatchersToJobOpsMgr</td>
    <td style="padding:15px">{base_path}/{version}/operations-manager/jobs/{pathv1}/add-watchers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">continueJobFromSpecifiedTask(jobId, body, callback)</td>
    <td style="padding:15px">continueJobFromSpecifiedTask</td>
    <td style="padding:15px">{base_path}/{version}/operations-manager/jobs/{pathv1}/continue?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">revertJobOpsMgr(jobId, body, callback)</td>
    <td style="padding:15px">revertJobOpsMgr</td>
    <td style="padding:15px">{base_path}/{version}/operations-manager/jobs/{pathv1}/revert?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEventDefinition(source, topic, callback)</td>
    <td style="padding:15px">OperationsManagerEventsBySourceAndTopic_GET</td>
    <td style="padding:15px">{base_path}/{version}/operations-manager/events/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retryTask(jobId, taskId, body, callback)</td>
    <td style="padding:15px">retryTask</td>
    <td style="padding:15px">{base_path}/{version}/operations-manager/jobs/{pathv1}/tasks/{pathv2}/retry?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">finishManualTaskOpsMgr(jobId, taskId, body, callback)</td>
    <td style="padding:15px">finishManualTaskOpsMgr</td>
    <td style="padding:15px">{base_path}/{version}/operations-manager/jobs/{pathv1}/tasks/{pathv2}/finish?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchIAP(body, callback)</td>
    <td style="padding:15px">searchIAP</td>
    <td style="padding:15px">{base_path}/{version}/search/find?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listOfServiceModels(callback)</td>
    <td style="padding:15px">listOfServiceModels</td>
    <td style="padding:15px">{base_path}/{version}/service_catalog/ServiceModels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserGroups(callback)</td>
    <td style="padding:15px">getUserGroups</td>
    <td style="padding:15px">{base_path}/{version}/service_catalog/getGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWorkflows(callback)</td>
    <td style="padding:15px">getWorkflows</td>
    <td style="padding:15px">{base_path}/{version}/service_catalog/GetWorkflows?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addServiceToCatalogStore(body, callback)</td>
    <td style="padding:15px">addServiceToCatalogStore</td>
    <td style="padding:15px">{base_path}/{version}/service_catalog/AddNewService?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateServiceCatalogStore(body, callback)</td>
    <td style="padding:15px">updateServiceCatalogStore</td>
    <td style="padding:15px">{base_path}/{version}/service_catalog/UpdateService?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCurrentUser(callback)</td>
    <td style="padding:15px">getCurrentUser</td>
    <td style="padding:15px">{base_path}/{version}/service_catalog/GetUserObject?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMultipleForms(callback)</td>
    <td style="padding:15px">getMultipleForms</td>
    <td style="padding:15px">{base_path}/{version}/service_catalog/GetForms?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNewServiceOrder(body, callback)</td>
    <td style="padding:15px">createNewServiceOrder</td>
    <td style="padding:15px">{base_path}/{version}/service_catalog/createServiceOrder?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">invokeServiceOrder(body, callback)</td>
    <td style="padding:15px">invokeServiceOrder</td>
    <td style="padding:15px">{base_path}/{version}/service_catalog/invokeServiceOrder?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importCatalogItems(body, callback)</td>
    <td style="padding:15px">importCatalogItems</td>
    <td style="padding:15px">{base_path}/{version}/service_catalog/catalog-items/import?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listServicesStore(filter, callback)</td>
    <td style="padding:15px">listServicesStore</td>
    <td style="padding:15px">{base_path}/{version}/service_catalog/ServiceCatalogStore/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFormIDForService(serviceId, callback)</td>
    <td style="padding:15px">getFormIDForService</td>
    <td style="padding:15px">{base_path}/{version}/service_catalog/GetFormId/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFormData(formId, callback)</td>
    <td style="padding:15px">getFormData</td>
    <td style="padding:15px">{base_path}/{version}/service_catalog/GetFormData/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteService(id, callback)</td>
    <td style="padding:15px">deleteService</td>
    <td style="padding:15px">{base_path}/{version}/service_catalog/deleteService/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exportCatalogItem(catalogItemId, callback)</td>
    <td style="padding:15px">exportCatalogItem</td>
    <td style="padding:15px">{base_path}/{version}/service_catalog/catalog-items/{pathv1}/export?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllServiceModelInformation(callback)</td>
    <td style="padding:15px">getAllServiceModelInformation.</td>
    <td style="padding:15px">{base_path}/{version}/service_management/service/models?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAllServiceModels(callback)</td>
    <td style="padding:15px">listAllServiceModels</td>
    <td style="padding:15px">{base_path}/{version}/service_management/service/models/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllServiceModelDetails(callback)</td>
    <td style="padding:15px">getAllServiceModelDetails</td>
    <td style="padding:15px">{base_path}/{version}/service_management/service/details?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceInstanceList(callback)</td>
    <td style="padding:15px">getServiceInstanceList</td>
    <td style="padding:15px">{base_path}/{version}/service_management/service/instances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceModelFields(callback)</td>
    <td style="padding:15px">getServiceModelFields</td>
    <td style="padding:15px">{base_path}/{version}/service_management/service/model/database?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setUpdateServiceModelFields(body, callback)</td>
    <td style="padding:15px">set/updateServiceModelFields</td>
    <td style="padding:15px">{base_path}/{version}/service_management/service/model/database?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getListOfMergedServiceModels(body, callback)</td>
    <td style="padding:15px">getListOfMergedServiceModels</td>
    <td style="padding:15px">{base_path}/{version}/service_management/service/models/update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setTenantForServiceInstance(body, callback)</td>
    <td style="padding:15px">setTenantForServiceInstance</td>
    <td style="padding:15px">{base_path}/{version}/service_management/service/setTenant?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">mapInstanceDataToServiceModel(body, callback)</td>
    <td style="padding:15px">mapInstanceDataToServiceModel</td>
    <td style="padding:15px">{base_path}/{version}/service_management/mapInstanceDataToModel?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">saveAnInstance(body, callback)</td>
    <td style="padding:15px">saveAnInstance</td>
    <td style="padding:15px">{base_path}/{version}/service_management/saveInstance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">saveInstances(body, callback)</td>
    <td style="padding:15px">saveInstances</td>
    <td style="padding:15px">{base_path}/{version}/service_management/saveInstances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">testAnInstance(body, callback)</td>
    <td style="padding:15px">testAnInstance</td>
    <td style="padding:15px">{base_path}/{version}/service_management/testInstance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configDryRun(body, callback)</td>
    <td style="padding:15px">configDryRun</td>
    <td style="padding:15px">{base_path}/{version}/service_management/testInstances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDetailsOfServiceModel(serviceModelPath, callback)</td>
    <td style="padding:15px">getDetailsOfServiceModel</td>
    <td style="padding:15px">{base_path}/{version}/service_management/model/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServiceModel(service, callback)</td>
    <td style="padding:15px">deleteServiceModel</td>
    <td style="padding:15px">{base_path}/{version}/service_management/service/model/database/remove/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllInstancesOfService(name, callback)</td>
    <td style="padding:15px">getAllInstancesOfService</td>
    <td style="padding:15px">{base_path}/{version}/service_management/service/instance/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceInstanceCheckSyncStatus(pathParam, body, callback)</td>
    <td style="padding:15px">getServiceInstanceCheck-SyncStatus</td>
    <td style="padding:15px">{base_path}/{version}/service_management/instance/{pathv1}/operations/check-sync?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceInstanceConfiguration(pathParam, body, callback)</td>
    <td style="padding:15px">getServiceInstanceConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/service_management/instance/{pathv1}/operations/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceInstanceDeviceModifications(pathParam, body, callback)</td>
    <td style="padding:15px">getServiceInstanceDeviceModifications</td>
    <td style="padding:15px">{base_path}/{version}/service_management/instance/{pathv1}/operations/device-modifications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">issueReactiveReDeployOnServiceInstance(pathParam, body, callback)</td>
    <td style="padding:15px">issueReactiveRe-deployOnServiceInstance</td>
    <td style="padding:15px">{base_path}/{version}/service_management/instance/{pathv1}/operations/reactive-redeploy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServiceInstances(name, body, callback)</td>
    <td style="padding:15px">deleteServiceInstances</td>
    <td style="padding:15px">{base_path}/{version}/service_management/service/instances/delete/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServiceInstancesUsingPaths(host, body, callback)</td>
    <td style="padding:15px">deleteServiceInstancesUsingPaths</td>
    <td style="padding:15px">{base_path}/{version}/service_management/deleteServicePaths/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dryRunDeleteServiceInstances(host, body, callback)</td>
    <td style="padding:15px">dryRunDeleteServiceInstances</td>
    <td style="padding:15px">{base_path}/{version}/service_management/deleteServicePathsDryRun/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addSaveServiceInstance(host, body, callback)</td>
    <td style="padding:15px">add/saveServiceInstance</td>
    <td style="padding:15px">{base_path}/{version}/service_management/service/instance/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">serviceInstanceDryRunWithFlags(host, body, callback)</td>
    <td style="padding:15px">serviceInstanceDryRunWithFlags</td>
    <td style="padding:15px">{base_path}/{version}/service_management/service/instance/{pathv1}/dryrun?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addSaveMultipleServiceInstances(host, body, callback)</td>
    <td style="padding:15px">add/saveMultipleServiceInstances</td>
    <td style="padding:15px">{base_path}/{version}/service_management/service/instances/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createServiceModelForm(serviceModelPath, callback)</td>
    <td style="padding:15px">createServiceModelForm</td>
    <td style="padding:15px">{base_path}/{version}/service_management/model/{pathv1}/form?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesServiceInstance(name, key, host, callback)</td>
    <td style="padding:15px">getDevicesServiceInstance</td>
    <td style="padding:15px">{base_path}/{version}/service_management/service/instance/devices/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServiceInstance(name, key, callback)</td>
    <td style="padding:15px">deleteServiceInstance</td>
    <td style="padding:15px">{base_path}/{version}/service_management/service/instance/delete/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServiceInstanceFromPath(servicePath, host, callback)</td>
    <td style="padding:15px">deleteServiceInstanceFromPath</td>
    <td style="padding:15px">{base_path}/{version}/service_management/deleteServicePath/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dryRunDeleteServiceInstanceFromPath(servicePath, host, callback)</td>
    <td style="padding:15px">dryRunDeleteServiceInstanceFromPath</td>
    <td style="padding:15px">{base_path}/{version}/service_management/deleteServicePathDryRun/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dryRunServiceInstance(host, outformat, body, callback)</td>
    <td style="padding:15px">dry-runServiceInstance</td>
    <td style="padding:15px">{base_path}/{version}/service_management/service/instance/{pathv1}/dryrun/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDetailsOfServiceInstance(model, value, callback)</td>
    <td style="padding:15px">getDetailsOfServiceInstance</td>
    <td style="padding:15px">{base_path}/{version}/service_management/getInstance/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTagDetailsGivenAnId(body, callback)</td>
    <td style="padding:15px">getTagDetailsGivenAnId</td>
    <td style="padding:15px">{base_path}/{version}/tags/get?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">findTagsByName(body, callback)</td>
    <td style="padding:15px">findTagsByName</td>
    <td style="padding:15px">{base_path}/{version}/tags/find?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllTags(callback)</td>
    <td style="padding:15px">getAllTags</td>
    <td style="padding:15px">{base_path}/{version}/tags/all?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNewTag(body, callback)</td>
    <td style="padding:15px">createNewTag</td>
    <td style="padding:15px">{base_path}/{version}/tags/create?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createManyNewTags(body, callback)</td>
    <td style="padding:15px">createManyNewTags</td>
    <td style="padding:15px">{base_path}/{version}/tags/createTags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">referenceTagToPeiceOfContent(body, callback)</td>
    <td style="padding:15px">referenceTagToPeiceOfContent</td>
    <td style="padding:15px">{base_path}/{version}/tags/createReference?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">handleReferences(body, callback)</td>
    <td style="padding:15px">create/updateReferencesForTags</td>
    <td style="padding:15px">{base_path}/{version}/tags/handleReferences?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTagsByContentId(body, callback)</td>
    <td style="padding:15px">getTagsByContentId</td>
    <td style="padding:15px">{base_path}/{version}/tags/getTagsByReference?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTagRefsByContentIds(body, callback)</td>
    <td style="padding:15px">getTagRefsByContentIds</td>
    <td style="padding:15px">{base_path}/{version}/tags/getTagReferences?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatingExistingTag(body, callback)</td>
    <td style="padding:15px">updatingExistingTag</td>
    <td style="padding:15px">{base_path}/{version}/tags/update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cloneExistingTag(body, callback)</td>
    <td style="padding:15px">cloneExistingTag</td>
    <td style="padding:15px">{base_path}/{version}/tags/duplicate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExistingTag(body, callback)</td>
    <td style="padding:15px">deleteExistingTag</td>
    <td style="padding:15px">{base_path}/{version}/tags/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">parseTemplates(body, callback)</td>
    <td style="padding:15px">parseTemplates.</td>
    <td style="padding:15px">{base_path}/{version}/template_builder/parseTemplate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applyTemplateUpdated2023(body, callback)</td>
    <td style="padding:15px">applyTemplateUpdated2023</td>
    <td style="padding:15px">{base_path}/{version}/template_builder/applyTemplate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applyTemplatesUpdated2023(body, callback)</td>
    <td style="padding:15px">applyTemplatesUpdated2023</td>
    <td style="padding:15px">{base_path}/{version}/template_builder/applyTemplates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applyTemplate(body, callback)</td>
    <td style="padding:15px">applyTemplate</td>
    <td style="padding:15px">{base_path}/{version}/template_builder/applyTemplate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applyTemplates(body, callback)</td>
    <td style="padding:15px">applyTemplates</td>
    <td style="padding:15px">{base_path}/{version}/template_builder/applyTemplates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">renderJinjaTemplate(name, body, callback)</td>
    <td style="padding:15px">renderJinjaTemplate</td>
    <td style="padding:15px">{base_path}/{version}/template_builder/templates/{pathv1}/renderJinja?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchTransformations(name, outgoing, schema, contains, containsField, equals, expand, limit, skip, callback)</td>
    <td style="padding:15px">searchTransformations.</td>
    <td style="padding:15px">{base_path}/{version}/transformations/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTransformation(body, callback)</td>
    <td style="padding:15px">createTransformation.</td>
    <td style="padding:15px">{base_path}/{version}/transformations/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importTransformation(body, callback)</td>
    <td style="padding:15px">importTransformation.</td>
    <td style="padding:15px">{base_path}/{version}/transformations/import?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTransformationDocumentById(id, callback)</td>
    <td style="padding:15px">getTransformationDocumentById.</td>
    <td style="padding:15px">{base_path}/{version}/transformations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSingleTransformation(id, body, callback)</td>
    <td style="padding:15px">updateSingleTransformation.</td>
    <td style="padding:15px">{base_path}/{version}/transformations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTransformation(id, callback)</td>
    <td style="padding:15px">deleteTransformation.</td>
    <td style="padding:15px">{base_path}/{version}/transformations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runTransformation(id, body, callback)</td>
    <td style="padding:15px">runTransformation.</td>
    <td style="padding:15px">{base_path}/{version}/transformations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTasks(callback)</td>
    <td style="padding:15px">getTasks</td>
    <td style="padding:15px">{base_path}/{version}/workflow_builder/tasks/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">calculateIncomingOutgoingSchemasForWorkflow(body, callback)</td>
    <td style="padding:15px">calculateIncoming/outgoingSchemasForWorkflow</td>
    <td style="padding:15px">{base_path}/{version}/workflow_builder/workflows/schemas?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addWorkflow(body, callback)</td>
    <td style="padding:15px">addWorkflow</td>
    <td style="padding:15px">{base_path}/{version}/workflow_builder/workflows/save?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">renameWorkflow(body, callback)</td>
    <td style="padding:15px">renameWorkflow</td>
    <td style="padding:15px">{base_path}/{version}/workflow_builder/workflows/rename?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exportWorkflow(body, callback)</td>
    <td style="padding:15px">exportWorkflow</td>
    <td style="padding:15px">{base_path}/{version}/workflow_builder/export?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importWorkflow(body, callback)</td>
    <td style="padding:15px">importWorkflow</td>
    <td style="padding:15px">{base_path}/{version}/workflow_builder/import?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteWorkflow(name, callback)</td>
    <td style="padding:15px">deleteWorkflow</td>
    <td style="padding:15px">{base_path}/{version}/workflow_builder/workflows/delete/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listGroupsForWorkflow(name, callback)</td>
    <td style="padding:15px">listGroupsForWorkflow</td>
    <td style="padding:15px">{base_path}/{version}/workflow_builder/workflows/{pathv1}/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">overwriteGroupsForWorkflow(name, body, callback)</td>
    <td style="padding:15px">overwriteGroupsForWorkflow</td>
    <td style="padding:15px">{base_path}/{version}/workflow_builder/workflows/{pathv1}/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addGroupToWorkflow(name, body, callback)</td>
    <td style="padding:15px">addGroupToWorkflow</td>
    <td style="padding:15px">{base_path}/{version}/workflow_builder/workflows/{pathv1}/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAllGroupsForWorkflow(name, callback)</td>
    <td style="padding:15px">deleteAllGroupsForWorkflow</td>
    <td style="padding:15px">{base_path}/{version}/workflow_builder/workflows/{pathv1}/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTaskDetailsWorkflowBuilder(app, task, callback)</td>
    <td style="padding:15px">getTaskDetailsWorkflowBuilder</td>
    <td style="padding:15px">{base_path}/{version}/workflow_builder/getTaskDetails/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeGroupFromWorkflow(name, group, callback)</td>
    <td style="padding:15px">removeGroupFromWorkflow</td>
    <td style="padding:15px">{base_path}/{version}/workflow_builder/workflows/{pathv1}/groups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">activateJobWorker(callback)</td>
    <td style="padding:15px">activateJobWorker</td>
    <td style="padding:15px">{base_path}/{version}/workflow_engine/jobWorker/activate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deactivateJobWorker(callback)</td>
    <td style="padding:15px">deactivateJobWorker</td>
    <td style="padding:15px">{base_path}/{version}/workflow_engine/jobWorker/deactivate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAggregateJobMetrics(order, sort, skip, limit, greaterThanEquals, greaterThanEqualsField, contains, containsField, callback)</td>
    <td style="padding:15px">getAggregateJobMetrics</td>
    <td style="padding:15px">{base_path}/{version}/workflow_engine/jobs/metrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAggregateTaskMetrics(order, sort, skip, limit, greaterThanEquals, greaterThanEqualsField, equals, equalsField, callback)</td>
    <td style="padding:15px">getAggregateTaskMetrics</td>
    <td style="padding:15px">{base_path}/{version}/workflow_engine/tasks/metrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCurrentAndFutureStatesOfTaskAndJobWorkers(callback)</td>
    <td style="padding:15px">getCurrentAndFutureStatesOfTaskAndJobWorkers</td>
    <td style="padding:15px">{base_path}/{version}/workflow_engine/workers/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAdapterModelTypes(contains, equals, startsWith, order, callback)</td>
    <td style="padding:15px">Get the list of available adapter model types.</td>
    <td style="padding:15px">{base_path}/{version}/adapter-models/types?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAdapter(body, callback)</td>
    <td style="padding:15px">Create a new adapter.</td>
    <td style="padding:15px">{base_path}/{version}/adapters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAdapters(contains, containsField, equals, equalsField, startsWith, startsWithField, skip, limit, sort, order, callback)</td>
    <td style="padding:15px">Get all adapters.</td>
    <td style="padding:15px">{base_path}/{version}/adapters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importAdapter(body, callback)</td>
    <td style="padding:15px">Import a new adapter.</td>
    <td style="padding:15px">{base_path}/{version}/adapters/import?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAdapter(name, callback)</td>
    <td style="padding:15px">Delete an adapter.</td>
    <td style="padding:15px">{base_path}/{version}/adapters/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAdapter(name, callback)</td>
    <td style="padding:15px">Get a single adapter.</td>
    <td style="padding:15px">{base_path}/{version}/adapters/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAdapter(name, body, callback)</td>
    <td style="padding:15px">Update an adapter.</td>
    <td style="padding:15px">{base_path}/{version}/adapters/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exportAdapter(name, callback)</td>
    <td style="padding:15px">Export a single adapter.</td>
    <td style="padding:15px">{base_path}/{version}/adapters/{pathv1}/export?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAdapterChangeLogs(name, release, callback)</td>
    <td style="padding:15px">Get the list of changelogs.</td>
    <td style="padding:15px">{base_path}/{version}/adapters/{pathv1}/changelogs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startAdapter(name, callback)</td>
    <td style="padding:15px">Start an adapter.</td>
    <td style="padding:15px">{base_path}/{version}/adapters/{pathv1}/start?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">stopAdapter(name, callback)</td>
    <td style="padding:15px">Stop an adapter.</td>
    <td style="padding:15px">{base_path}/{version}/adapters/{pathv1}/stop?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">restartAdapter(name, callback)</td>
    <td style="padding:15px">Restart an adapter.</td>
    <td style="padding:15px">{base_path}/{version}/adapters/{pathv1}/restart?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAdapterLogging(name, body, callback)</td>
    <td style="padding:15px">Update the log levels.</td>
    <td style="padding:15px">{base_path}/{version}/adapters/{pathv1}/loglevel?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAdapterProperties(name, body, callback)</td>
    <td style="padding:15px">Update the properties of an adapter.</td>
    <td style="padding:15px">{base_path}/{version}/adapters/{pathv1}/properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBrokerMap(callback)</td>
    <td style="padding:15px">Get a mapping between brokers and adapters.</td>
    <td style="padding:15px">{base_path}/{version}/adapters/brokers/mapping?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplication(name, callback)</td>
    <td style="padding:15px">Get a single application.</td>
    <td style="padding:15px">{base_path}/{version}/applications/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateApplication(name, body, callback)</td>
    <td style="padding:15px">Update an application.</td>
    <td style="padding:15px">{base_path}/{version}/applications/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplicationChangelogs(name, release, callback)</td>
    <td style="padding:15px">Get the list of changelogs.</td>
    <td style="padding:15px">{base_path}/{version}/applications/{pathv1}/changelogs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllApplications(contains, containsField, equals, equalsField, startsWith, startsWithField, skip, limit, sort, order, callback)</td>
    <td style="padding:15px">Get all applications.</td>
    <td style="padding:15px">{base_path}/{version}/applications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startApplication(name, callback)</td>
    <td style="padding:15px">Start an application.</td>
    <td style="padding:15px">{base_path}/{version}/applications/{pathv1}/start?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">stopApplication(name, callback)</td>
    <td style="padding:15px">Stop an application.</td>
    <td style="padding:15px">{base_path}/{version}/applications/{pathv1}/stop?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">restartApplication(name, callback)</td>
    <td style="padding:15px">Restart an application.</td>
    <td style="padding:15px">{base_path}/{version}/applications/{pathv1}/restart?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateApplicationLogging(name, body, callback)</td>
    <td style="padding:15px">Update the log levels.</td>
    <td style="padding:15px">{base_path}/{version}/applications/{pathv1}/loglevel?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateApplicationProperties(name, body, callback)</td>
    <td style="padding:15px">Update the properties of an application.</td>
    <td style="padding:15px">{base_path}/{version}/applications/{pathv1}/properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">whoAmi(roles, callback)</td>
    <td style="padding:15px">Gets the authorization data for the logged in user</td>
    <td style="padding:15px">{base_path}/{version}/whoami?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMySession(callback)</td>
    <td style="padding:15px">Gets the session document for the active user</td>
    <td style="padding:15px">{base_path}/{version}/mySession?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMyPrincipal(callback)</td>
    <td style="padding:15px">Gets the principal document for the active user</td>
    <td style="padding:15px">{base_path}/{version}/myPrincipal?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMyTtl(callback)</td>
    <td style="padding:15px">Get the number of seconds until the logged in user's session expires.</td>
    <td style="padding:15px">{base_path}/{version}/myTtl?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">login(body, callback)</td>
    <td style="padding:15px">Log in an create a session in IAP</td>
    <td style="padding:15px">{base_path}/{version}/login?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">logout(callback)</td>
    <td style="padding:15px">Log out of IAP and delete current session</td>
    <td style="padding:15px">{base_path}/{version}/logout?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadPublicFile(fileName, callback)</td>
    <td style="padding:15px">Download a public file from IAP</td>
    <td style="padding:15px">{base_path}/{version}/download?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemId(callback)</td>
    <td style="padding:15px">Get system server id.</td>
    <td style="padding:15px">{base_path}/{version}/system/id?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHealthStatus(callback)</td>
    <td style="padding:15px">Get status.</td>
    <td style="padding:15px">{base_path}/{version}/health/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReleaseVersion(callback)</td>
    <td style="padding:15px">Get verion.</td>
    <td style="padding:15px">{base_path}/{version}/version?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplicationConfigs(callback)</td>
    <td style="padding:15px">Get all application configs in IAP.</td>
    <td style="padding:15px">{base_path}/{version}/config/apps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserProfile(username, callback)</td>
    <td style="padding:15px">Gets the user profile for the active user</td>
    <td style="padding:15px">{base_path}/{version}/profile/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateUserProfile(body, callback)</td>
    <td style="padding:15px">Updates the user profile for the active user</td>
    <td style="padding:15px">{base_path}/{version}/profile/update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccounts(multiContains, multiEquals, multiStartsWith, inactive, isServiceAccount, groupId, skip, limit, sort, order, callback)</td>
    <td style="padding:15px">Get all accounts.</td>
    <td style="padding:15px">{base_path}/{version}/authorization/accounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccount(accountId, callback)</td>
    <td style="padding:15px">Get an individual account.</td>
    <td style="padding:15px">{base_path}/{version}/authorization/accounts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAccount(accountId, body, callback)</td>
    <td style="padding:15px">Update an account.</td>
    <td style="padding:15px">{base_path}/{version}/authorization/accounts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroups(multiContains, multiEquals, multiStartsWith, inactive, skip, limit, sort, order, refresh, callback)</td>
    <td style="padding:15px">Get a list of groups.</td>
    <td style="padding:15px">{base_path}/{version}/authorization/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createGroup(body, callback)</td>
    <td style="padding:15px">Create a group.</td>
    <td style="padding:15px">{base_path}/{version}/authorization/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroupsShortGroups(multiContains, multiEquals, multiStartsWith, inactive, skip, limit, sort, order, refresh, callback)</td>
    <td style="padding:15px">Get a simplified list of groups.</td>
    <td style="padding:15px">{base_path}/{version}/authorization/groups/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroup(groupId, callback)</td>
    <td style="padding:15px">Get an individual group.</td>
    <td style="padding:15px">{base_path}/{version}/authorization/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateGroup(groupId, body, callback)</td>
    <td style="padding:15px">Update a group.</td>
    <td style="padding:15px">{base_path}/{version}/authorization/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteGroup(groupId, callback)</td>
    <td style="padding:15px">Delete a group.</td>
    <td style="padding:15px">{base_path}/{version}/authorization/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRoles(multiContains, multiEquals, multiStartsWith, inactive, skip, limit, sort, order, callback)</td>
    <td style="padding:15px">Get a list of roles.</td>
    <td style="padding:15px">{base_path}/{version}/authorization/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRole(body, callback)</td>
    <td style="padding:15px">Create a role.</td>
    <td style="padding:15px">{base_path}/{version}/authorization/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRole(roleId, callback)</td>
    <td style="padding:15px">Get an individual role.</td>
    <td style="padding:15px">{base_path}/{version}/authorization/roles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRole(roleId, body, callback)</td>
    <td style="padding:15px">Update a role.</td>
    <td style="padding:15px">{base_path}/{version}/authorization/roles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRole(roleId, callback)</td>
    <td style="padding:15px">Delete a role.</td>
    <td style="padding:15px">{base_path}/{version}/authorization/roles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMethods(multiContains, multiEquals, multiStartsWith, skip, limit, sort, order, callback)</td>
    <td style="padding:15px">Get a list of methods.</td>
    <td style="padding:15px">{base_path}/{version}/authorization/methods?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getViews(multiContains, multiEquals, multiStartsWith, skip, limit, sort, order, callback)</td>
    <td style="padding:15px">Get a list of views.</td>
    <td style="padding:15px">{base_path}/{version}/authorization/views?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceCounts(skip, limit, order, serverId, before, after, callback)</td>
    <td style="padding:15px">Get device count history</td>
    <td style="padding:15px">{base_path}/{version}/device-counts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLinks(multiContains, multiEquals, multiStartsWith, skip, limit, sort, order, callback)</td>
    <td style="padding:15px">Get all links.</td>
    <td style="padding:15px">{base_path}/{version}/external-links?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createLink(body, callback)</td>
    <td style="padding:15px">Update a link.</td>
    <td style="padding:15px">{base_path}/{version}/external-links?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLink(linkId, callback)</td>
    <td style="padding:15px">Get an individual link.</td>
    <td style="padding:15px">{base_path}/{version}/external-links/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchUpdatealink(linkId, body, callback)</td>
    <td style="padding:15px">Update a link.</td>
    <td style="padding:15px">{base_path}/{version}/external-links/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteLink(linkId, callback)</td>
    <td style="padding:15px">Delete a link.</td>
    <td style="padding:15px">{base_path}/{version}/external-links/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAdapterHealth(name, callback)</td>
    <td style="padding:15px">Get the health of a single adapter.</td>
    <td style="padding:15px">{base_path}/{version}/health/adapters/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplicationHealth(name, callback)</td>
    <td style="padding:15px">Get the health of a single application.</td>
    <td style="padding:15px">{base_path}/{version}/health/applications/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAdaptersHealth(contains, containsField, equals, equalsField, startsWith, startsWithField, skip, limit, sort, order, callback)</td>
    <td style="padding:15px">Get the health of all the adapters.</td>
    <td style="padding:15px">{base_path}/{version}/health/adapters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplicationsHealth(contains, containsField, equals, equalsField, startsWith, startsWithField, skip, limit, sort, order, callback)</td>
    <td style="padding:15px">Get the health of all the applications.</td>
    <td style="padding:15px">{base_path}/{version}/health/applications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServerHealth(callback)</td>
    <td style="padding:15px">Get the health of the server.</td>
    <td style="padding:15px">{base_path}/{version}/health/server?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemHealth(callback)</td>
    <td style="padding:15px">Get the health of the system.</td>
    <td style="padding:15px">{base_path}/{version}/health/system?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAdaptersHtml(callback)</td>
    <td style="padding:15px">Generate documentation for adapters</td>
    <td style="padding:15px">{base_path}/{version}/help/adapters/html?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBrokersHtml(callback)</td>
    <td style="padding:15px">Generate documentation for brokers</td>
    <td style="padding:15px">{base_path}/{version}/help/brokers/html?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplicationsHtml(callback)</td>
    <td style="padding:15px">Generate documentation for applications</td>
    <td style="padding:15px">{base_path}/{version}/help/applications/html?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOpenApi(url, callback)</td>
    <td style="padding:15px">Generate an OpenAPI v3 document for IAP</td>
    <td style="padding:15px">{base_path}/{version}/help/openapi?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIndexes(application, callback)</td>
    <td style="padding:15px">Get all pre-defined indexes.</td>
    <td style="padding:15px">{base_path}/{version}/indexes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkIndexes(collection, callback)</td>
    <td style="padding:15px">Check the status of a collection's indexes in Mongo</td>
    <td style="padding:15px">{base_path}/{version}/indexes/{pathv1}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkMultipleIndexes(collections, callback)</td>
    <td style="padding:15px">Check the status of multiple collections' indexes in Mongo</td>
    <td style="padding:15px">{base_path}/{version}/indexes/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIndexes(collection, callback)</td>
    <td style="padding:15px">Create a collection's indexes in Mongo</td>
    <td style="padding:15px">{base_path}/{version}/indexes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIntegrationModel(body, callback)</td>
    <td style="padding:15px">Create a new integration model.</td>
    <td style="padding:15px">{base_path}/{version}/integration-models?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIntegrationModels(contains, containsField, equals, equalsField, startsWith, startsWithField, skip, limit, sort, order, callback)</td>
    <td style="padding:15px">Get all integration models.</td>
    <td style="padding:15px">{base_path}/{version}/integration-models?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIntegrationModel(body, callback)</td>
    <td style="padding:15px">Update an integration model.</td>
    <td style="padding:15px">{base_path}/{version}/integration-models?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIntegrationModel(name, callback)</td>
    <td style="padding:15px">Delete an integration model.</td>
    <td style="padding:15px">{base_path}/{version}/integration-models/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIntegrationModel(name, callback)</td>
    <td style="padding:15px">Get a specific integration model.</td>
    <td style="padding:15px">{base_path}/{version}/integration-models/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exportIntegrationModel(name, callback)</td>
    <td style="padding:15px">Export an integration model.</td>
    <td style="padding:15px">{base_path}/{version}/integration-models/{pathv1}/export?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">validateIntegrationModel(body, callback)</td>
    <td style="padding:15px">Validate an integration model.</td>
    <td style="padding:15px">{base_path}/{version}/integration-models/validation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIntegration(body, callback)</td>
    <td style="padding:15px">Create a new integration.</td>
    <td style="padding:15px">{base_path}/{version}/integrations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIntegrations(contains, containsField, equals, equalsField, startsWith, startsWithField, skip, limit, sort, order, callback)</td>
    <td style="padding:15px">Get all integrations.</td>
    <td style="padding:15px">{base_path}/{version}/integrations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIntegration(name, callback)</td>
    <td style="padding:15px">Delete an integration.</td>
    <td style="padding:15px">{base_path}/{version}/integrations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIntegration(name, callback)</td>
    <td style="padding:15px">Get a single integration.</td>
    <td style="padding:15px">{base_path}/{version}/integrations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIntegration(name, body, callback)</td>
    <td style="padding:15px">Update an integration.</td>
    <td style="padding:15px">{base_path}/{version}/integrations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIntegrationProperties(name, body, callback)</td>
    <td style="padding:15px">Update the properties of an integration.</td>
    <td style="padding:15px">{base_path}/{version}/integrations/{pathv1}/properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchResourceModels(callback)</td>
    <td style="padding:15px">Searches resource models</td>
    <td style="padding:15px">{base_path}/{version}/lifecycle-manager/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getActionExecutions(callback)</td>
    <td style="padding:15px">Searches resource action history documents</td>
    <td style="padding:15px">{base_path}/{version}/lifecycle-manager/action-executions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getResourceInstancesHttp(modelId, callback)</td>
    <td style="padding:15px">Searches resource instances</td>
    <td style="padding:15px">{base_path}/{version}/lifecycle-manager/resources/{pathv1}/instances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createServiceAccount(body, callback)</td>
    <td style="padding:15px">Create a service account in the database.</td>
    <td style="padding:15px">{base_path}/{version}/oauth/serviceAccounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceAccounts(multiContains, multiEquals, multiStartsWith, skip, limit, sort, order, callback)</td>
    <td style="padding:15px">Get service accounts from the database.</td>
    <td style="padding:15px">{base_path}/{version}/oauth/serviceAccounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateServiceAccount(clientId, body, callback)</td>
    <td style="padding:15px">Updates a service account in the database.</td>
    <td style="padding:15px">{base_path}/{version}/oauth/serviceAccounts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServiceAccount(clientId, callback)</td>
    <td style="padding:15px">Deletes a service account in the database.</td>
    <td style="padding:15px">{base_path}/{version}/oauth/serviceAccounts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">regenerateSecret(clientId, callback)</td>
    <td style="padding:15px">Regenerates the client_secret and returns the new value</td>
    <td style="padding:15px">{base_path}/{version}/oauth/serviceAccounts/{pathv1}/regenerate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">machineAuthentication(body, callback)</td>
    <td style="padding:15px">Retrieve a JWT from a client's credentials</td>
    <td style="padding:15px">{base_path}/{version}/oauth/token?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exportPrebuilt(id, callback)</td>
    <td style="padding:15px">Export a prebuilt</td>
    <td style="padding:15px">{base_path}/{version}/prebuilts/{pathv1}/export?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPrebuilt(id, callback)</td>
    <td style="padding:15px">Get a single prebuilt.</td>
    <td style="padding:15px">{base_path}/{version}/prebuilts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removePrebuilt(id, callback)</td>
    <td style="padding:15px">Remove a prebuilt.</td>
    <td style="padding:15px">{base_path}/{version}/prebuilts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePrebuilt(id, body, callback)</td>
    <td style="padding:15px">Update a prebuilt</td>
    <td style="padding:15px">{base_path}/{version}/prebuilts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPrebuilts(contains, containsField, equals, equalsField, startsWith, startsWithField, skip, limit, sort, order, config, callback)</td>
    <td style="padding:15px">Get all the prebuilts.</td>
    <td style="padding:15px">{base_path}/{version}/prebuilts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importPrebuilt(body, callback)</td>
    <td style="padding:15px">Import a prebuilt.</td>
    <td style="padding:15px">{base_path}/{version}/prebuilts/import?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">validatePrebuilt(body, callback)</td>
    <td style="padding:15px">Validate the data structure of a prebuilt.</td>
    <td style="padding:15px">{base_path}/{version}/prebuilts/import/validation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRepositoryConfig(body, callback)</td>
    <td style="padding:15px">Create repository configuration</td>
    <td style="padding:15px">{base_path}/{version}/prebuilts-repository/configs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositoryConfigurations(contains, containsField, equals, equalsField, startsWith, startsWithField, sort, order, callback)</td>
    <td style="padding:15px">Get Repository Configurations</td>
    <td style="padding:15px">{base_path}/{version}/prebuilts-repository/configs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRepositoryConfig(name, callback)</td>
    <td style="padding:15px">Delete repository configuration</td>
    <td style="padding:15px">{base_path}/{version}/prebuilts-repository/configs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRepositoryConfig(name, body, callback)</td>
    <td style="padding:15px">Update repository configuration</td>
    <td style="padding:15px">{base_path}/{version}/prebuilts-repository/configs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositoryPrebuilts(page, order, iapVersion, search, config, versionStatus, callback)</td>
    <td style="padding:15px">Get all prebuilts</td>
    <td style="padding:15px">{base_path}/{version}/prebuilts-repository?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createProfile(body, callback)</td>
    <td style="padding:15px">Creates a new profile</td>
    <td style="padding:15px">{base_path}/{version}/profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProfiles(contains, containsField, equals, equalsField, startsWith, startsWithField, skip, limit, sort, order, callback)</td>
    <td style="padding:15px">Get all profiles.</td>
    <td style="padding:15px">{base_path}/{version}/profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importProfile(body, callback)</td>
    <td style="padding:15px">Import a new profile</td>
    <td style="padding:15px">{base_path}/{version}/profiles/import?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProfile(id, callback)</td>
    <td style="padding:15px">Delete a profile.</td>
    <td style="padding:15px">{base_path}/{version}/profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProfile(id, callback)</td>
    <td style="padding:15px">Get a single profile.</td>
    <td style="padding:15px">{base_path}/{version}/profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateProfile(id, body, callback)</td>
    <td style="padding:15px">Updates a profile.</td>
    <td style="padding:15px">{base_path}/{version}/profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exportProfile(id, callback)</td>
    <td style="padding:15px">Export a single profile.</td>
    <td style="padding:15px">{base_path}/{version}/profiles/{pathv1}/export?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">switchActiveProfile(id, callback)</td>
    <td style="padding:15px">Switch the active profile.</td>
    <td style="padding:15px">{base_path}/{version}/profiles/{pathv1}/active?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPrometheusMetrics(callback)</td>
    <td style="padding:15px">Get prometheus metrics.</td>
    <td style="padding:15px">{base_path}/{version}/prometheus_metrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAdapterSchema(name, callback)</td>
    <td style="padding:15px">Get the adapter schema.</td>
    <td style="padding:15px">{base_path}/{version}/schema/adapters/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplicationSchema(name, callback)</td>
    <td style="padding:15px">Get the application schema.</td>
    <td style="padding:15px">{base_path}/{version}/schema/applications/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIntegrationSchema(name, callback)</td>
    <td style="padding:15px">Get the integration schema.</td>
    <td style="padding:15px">{base_path}/{version}/schema/integrations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProfileSchema(callback)</td>
    <td style="padding:15px">Get the profile schema.</td>
    <td style="padding:15px">{base_path}/{version}/schema/profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
