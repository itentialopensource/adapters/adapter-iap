
## 0.6.4 [10-15-2024]

* Changes made at 2024.10.14_21:02PM

See merge request itentialopensource/adapters/adapter-iap!26

---

## 0.6.3 [08-27-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-iap!24

---

## 0.6.2 [08-14-2024]

* Changes made at 2024.08.14_19:23PM

See merge request itentialopensource/adapters/adapter-iap!23

---

## 0.6.1 [08-07-2024]

* Changes made at 2024.08.06_21:05PM

See merge request itentialopensource/adapters/adapter-iap!22

---

## 0.6.0 [07-25-2024]

* 2024 Migration

See merge request itentialopensource/adapters/adapter-iap!21

---

## 0.5.10 [03-27-2024]

* fixing tabs

See merge request itentialopensource/adapters/adapter-iap!20

---

## 0.5.9 [03-26-2024]

* fixing tabs

See merge request itentialopensource/adapters/adapter-iap!20

---

## 0.5.8 [03-25-2024]

* fixing tabs

See merge request itentialopensource/adapters/adapter-iap!20

---

## 0.5.7 [03-25-2024]

* fixing tabs

See merge request itentialopensource/adapters/adapter-iap!20

---

## 0.5.6 [03-25-2024]

* fixing tabs

See merge request itentialopensource/adapters/adapter-iap!20

---

## 0.5.5 [03-25-2024]

* fixing tabs

See merge request itentialopensource/adapters/adapter-iap!20

---

## 0.5.4 [03-25-2024]

* fixing tabs

See merge request itentialopensource/adapters/adapter-iap!20

---

## 0.5.3 [03-25-2024]

* fixing tab

See merge request itentialopensource/adapters/adapter-iap!20

---

## 0.5.2 [02-26-2024]

* Changes made at 2024.02.26_11:05AM

See merge request itentialopensource/adapters/adapter-iap!16

---

## 0.5.1 [12-24-2023]

* update axios and metadata

See merge request itentialopensource/adapters/adapter-iap!15

---

## 0.5.0 [12-14-2023]

* 2023 Adapter Migration + Updates for support for multiple IAP Versions

See merge request itentialopensource/adapters/adapter-iap!14

---

## 0.4.0 [11-13-2023]

* 2023 Adapter Migration + Updates for support for multiple IAP Versions

See merge request itentialopensource/adapters/adapter-iap!14

---

## 0.3.0 [11-13-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/adapter-iap!13

---

## 0.2.3 [08-04-2021]

* Update sampleProperties.json

See merge request itentialopensource/adapters/adapter-iap!5

---

## 0.2.2 [03-07-2021]

* migration to the latest adapter foundation - and healthcheck change

See merge request itentialopensource/adapters/adapter-iap!4

---

## 0.2.1 [07-08-2020]

* migration

See merge request itentialopensource/adapters/adapter-iap!2

---

## 0.2.0 [06-03-2020]

* Minor/adapt 199

See merge request itentialopensource/adapters/adapter-iap!1

---

## 0.1.1 [03-26-2020]

* Bug fixes and performance improvements

See commit f84b907

---
